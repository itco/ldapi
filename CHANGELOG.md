# Change Log #


## In progress ##

 * ...
  
## v2.1.0 ##

 * Removed gender field, added deprecation warning to get method

## v2.0.5 ##

* Added support to nullify fields

## v2.0.4 ##

 * Added `getCurrentClass` static method
 * Added const list for gender

## v2.0.4 ##

 * Improved IBAN validation, so no warnings are thrown

## v2.0.2 ##

 * Brought back composer.lock, needed for buildpipeline it seems

## v2.0.1 ##

 * Removed `ext-ldap` requirement from composer.json (not needed for tests)

## v2.0.0 ##

(This version might still be compatible with v1)

 * Removed trailing mercurial files
 * Added this changelog
 * Implemented code coverage and autodocs
 * Improved doc blocks
 * Replaced hard-coded class names by magic constants
 * Moved as much as possible to LDAPObject base class
 * Added validation for committee aliasses
 * Increased test coverage

## v1.0.1 ##

 * Improved doc blocks
 * Removed trailing debug statements

## v1.0.0 ##

 * Complete implementation of unit tests (no effect on regular usage).
 * Added implicit linking to original LDAPI instance to prevent having to pass it as an argument. This release was originally planned as v0.3.0, however, since it is a breaking update, v1.0.0 is more appropriate.

## v0.2.8 ##

 * Last version before unit testing, currently mostly used.
