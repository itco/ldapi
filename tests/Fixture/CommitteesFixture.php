<?php

namespace LDAPI\Test\Fixture;

class CommitteesFixture
{
    /*
      dn: cn=itco,ou=Group,dc=astaat,dc=tnw,dc=utwente,dc=nl
    cn: itco
    description: com
    objectClass: posixGroup
    gidNumber: 65535
    memberUid: maximilian.stein
    memberUid: geert.folkertsma
    memberUid: carlmaykel.orman
    memberUid: riccardo.sneep
     */

    static $base_user = [
        'gidNumber' => '65000',
        'cn' => 'itco',
        'description' => 'com',
        'objectClass' => 'posixGroup',
        //'memberUid' => ['piet.tester']
    ];

    public static function getCommittees()
    {
        $id = 65000;

        return [
            [
                'cn' => 'itco',
                'gidNumber' => $id++,
                'memberUid' => ['piet.tester', 'robert.roos']
            ],
            [
                'cn' => 'board',
                'gidNumber' => $id++,
                'memberUid' => ['piet.tester', 'piet2.tester', 'bob.marly']
            ],
            [
                'cn' => 'bucom',
                'gidNumber' => $id++, // No members
            ],
            [
                'cn' => 'kitcat',
                'gidNumber' => $id++,
                'memberUid' => ['piet.tester', 'non.existent']
            ],
            [
                'cn' => 'kitcatalias',
                'gidNumber' => $id++,
                'memberUid' => ['kitcat', 'otherkitcatalias']
            ],
            [
                'cn' => 'circularalias1',
                'gidNumber' => $id++,
                'memberUid' => ['circularalias2']
            ],
            [
                'cn' => 'circularalias2',
                'gidNumber' => $id++,
                'memberUid' => ['circularalias1']
            ],
        ];
    }

    /**
     * Get array with fixture data
     * 
     * @return array
     */
    static function getData()
    {
        $committees = [];

        foreach (self::getCommittees() as $committee)
        {
            $new_committee = array_merge(self::$base_user, $committee);

            $committees[] = $new_committee;
        }

        return $committees;
    }

}
