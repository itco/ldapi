<?php

namespace LDAPI\Test\Fixture;

class UsersFixture
{
    /*
      dn: uid=robert.roos,ou=People,dc=astaat,dc=tnw,dc=utwente,dc=nl
      gidNumber: 100
      objectClass: Astatine
      objectClass: organizationalPerson
      objectClass: posixAccount
      jpegPhoto:: junk
      homeDirectory: /home/robert.roos
      c: nl
      cn: Robert
      givenName: Robert Andreas
      initials: R.A.
      joinDate: 2014-09-01
      l: Enschede
      lidMaatschap: 0
      lidNummer: 0
      mail: r.a.roos@student.utwente.nl
      mailVoorVoegsel: robert.roos
      mailVoorVoegsel: roosje
      mailVoorVoegsel: ssa-secretary
      mailVoorVoegsel: bob
      postalAddress: Laaressingel 178
      postalCode: 7514EW
      sn: Roos
      studentNummer: 1589482
      uid: robert.roos
      uidNumber: 3695
      IBANrekNummer: NL48INGB0001375586
      lichting: 11
      machtiging: 4
      mobile: 0615638335
      nationaliteit: nl
      telephoneNumber: 0294285466
      geboorteDatum: 1996-12-02
      ATtentie: 1
      BIC: INGBNL2A
      userPassword: {CRYPT}$6$rounds=5000$ev5evFlGGsniJCwq$ecAJcT4U733xJMdZ5HmGkXTGYBt
      UqC67/7p67PM0Ge/OTRfDozOs80BvyhVXNiVy6E1qXTgyFsMWjlRgerPwn0
     */

    static $base_user = [
        'gidNumber' => '100',
        'objectClass' => ['Astatine', 'organizationalPerson', 'posixAccount'],
        'c' => 'nl',
        'cn' => 'Piet',
        'givenName' => 'Piet',
        'initials' => 'P.',
        'joinDate' => '2014-09-01',
        'l' => 'Enschede',
        'lidMaatschap' => '0',
        'lidNummer' => '0',
        'mail' => 'itco@gmail.com',
        'mailVoorVoegsel' => ['alias'],
        'postalAddress' => 'Horst 2',
        'postalCode' => '7522LW',
        'sn' => 'Tester',
        'studentNummer' => '1512345',
        'uid' => 'piet.tester',
        'uidNumber' => 0,
        'IBANrekNummer' => 'NL48INGB0001234567',
        'lichting' => '1',
        'machtiging' => '4',
        'mobile' => '0612345678',
        'nationaliteit' => 'nl',
        'telephoneNumber' => '0201234567',
        'geboorteDatum' => '1996-01-01',
        'ATtentie' => '1',
        'BIC' => 'INGBNL2A',
        'userPassword' => '{CRYPT}$6$a7ed326307f805d5$tWVwDYYwVatCD71hKWk9xbVUSHuDlMR4lLCFmcYsx6pm9pw4rq5zfmzI1EIAEotUWDMgJUdZ5YazIte4k/W/Y.' // "wachtwoord"
    ];

    public static function getUsers()
    {
        $id = 3000;

        return [
            [
                'uid' => 'piet.tester',
                'uidNumber' => $id++,
                'studentNummer' => '1234567',
            ],
            [
                'uid' => 'piet2.tester',
                'uidNumber' => $id++,
                'studentNummer' => '1512345',
            ],
            [
                'uid' => 'robert.roos',
                'cn' => 'Robert',
                'sn' => 'Roos',
                'initials' => 'R.A.',
                'givenName' => 'Robert Andreas',
                'uidNumber' => $id++,
                'studentNummer' => '1589482',
            ],
            [
                'uid' => 'bob.marly',
                'cn' => 'Bob',
                'sn' => 'Marly',
                'initials' => 'B.S.D.',
                'givenName' => 'Bob Sob Dob',
                'uidNumber' => $id++,
                'studentNummer' => '1589000',
            ],
        ];
    }

    /**
     * Get array with fixture data
     * 
     * @return array
     */
    static function getData()
    {
        $users = [];

        foreach (self::getUsers() as $user)
        {
            $new_user = array_merge(self::$base_user, $user);

            $new_user = self::buildUid($new_user);

            $users[] = $new_user;
        }

        return $users;
    }

    /**
     * Set uid in other fields where needed
     * 
     * @param type $user
     * @return type
     */
    static function buildUid($user)
    {
        $uid = $user['uid'];

        $user['homeDirectory'] = '/home/' . $uid;
        $user['mailVoorVoegsel'][] = $uid;

        return $user;
    }

}
