<?php

namespace LDAPI\Test\Fixture;

class ForwardsFixture
{
    /*
    dn: cn=piet.tester,ou=forward,dc=astaat,dc=tnw,dc=utwente,dc=nl
    cn: piet.tester
    objectClass: organizationalPerson
    objectClass: top
    postOfficeBox: piet.tester@gmail.com
    sn: forward
     */

    static $base_forward = [
        'cn' => 'piet.tester',
        'objectClass' => ['organizationalPerson', 'top'],
        'postOfficeBox' => 'piet.tester@gmail.com',
        'sn' => 'forward'
    ];

    public static function getForwards()
    {
        return [
            [
                'cn' => 'piet.tester',
                'postOfficeBox' => 'piet.tester@gmail.com',
            ],
        ];
    }

    /**
     * Get array with fixture data
     * 
     * @return array
     */
    static function getData()
    {
        $forwards = [];

        foreach (self::getForwards() as $forward)
        {
            $new_forward = array_merge(self::$base_forward, $forward);

            $forwards[] = $new_forward;
        }

        return $forwards;
    }

}
