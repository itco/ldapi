<?php

namespace LDAPI\Test\TestCase;

use LDAPI\LDAPI;

/**
 * Test the committee methods of the LDAPI class
 */
class LDAPICommitteesTest extends LDAPITestCase
{

    const COMMITTEE_DATA = [
        'name' => 'myfirstcommittee'
    ];
    
    public $fixtures = [
        'Committees'
    ];

    /**
     * Test single committee retrieval
     */
    public function testGetCommittee()
    {
        $id = 65000;

        $committee = $this->ldapi->getCommittee($id);

        $this->assertTypeCommittee($committee);
        $this->assertEquals($id, $committee->id);
        $this->assertEquals('itco', $committee->name);
    }

    /**
     * Test single committee retrieval when not found
     */
    public function testGetCommitteeNotFound()
    {
        $id = 9999;

        $committee = $this->ldapi->getCommittee($id);

        $this->assertFalse($committee);
    }

    /**
     * Test multiple committee retrieval by id
     */
    public function testGetCommittees()
    {
        $ids = [65000, 65001];

        $committees = $this->ldapi->getCommittees($ids);

        $this->assertArrayNotEmpty($committees);

        foreach ($committees as $id => $committee)
        {
            $this->assertTypeCommittee($committee);
            $this->assertTrue(in_array($committee->id, $ids));
            $this->assertEquals($id, $committee->id);
        }
    }

    /**
     * Test multiple committee retrieval when at least one not found
     */
    public function testGetCommitteesNotFound()
    {
        $ids = [65000, 9999];

        $committees = $this->ldapi->getCommittees($ids);

        $this->assertArrayNotEmpty($committees);

        foreach ($committees as $id => $committee)
        {
            $this->assertTypeCommittee($committee);
            $this->assertTrue(in_array($committee->id, $ids));
            $this->assertNotEquals(9999, $committee->id);
            $this->assertEquals($id, $committee->id);
        }
    }

    /**
     * Test getting a single committee by name
     */
    public function testGetCommitteeByName()
    {
        $name = 'itco';

        $committee = $this->ldapi->getCommitteeByName($name);

        $this->assertTypeCommittee($committee);
        $this->assertEquals($name, $committee->name);
    }

    /**
     * Test search for a committee
     */
    public function testFindCommitteesByValues()
    {
        $committees = $this->ldapi->findCommitteesByValues(['name' => 'it*']);

        $this->assertArrayNotEmpty($committees);

        foreach ($committees as $id => $committee)
        {
            $this->assertTypeCommittee($committee);
            $this->assertEquals('itco', $committee->name);
            $this->assertEquals($id, $committee->id);
        }
    }

    /**
     * Test searching for a committee when nothing is found
     */
    public function testFindCommitteesByValuesEmpty()
    {
        $committees = $this->ldapi->findCommitteesByValues(['name' => 'i_dont_exist']);

        $this->assertArrayEmpty($committees);
    }

    /**
     * Test retrieving all committees
     */
    public function testFindAllCommittees()
    {
        $committees = $this->ldapi->findAllCommittees();

        $this->assertArrayNotEmpty($committees);

        foreach ($committees as $id => $committee)
        {
            $this->assertTypeCommittee($committee);
            $this->assertEquals($id, $committee->id);
        }
    }

    /**
     * Test creation of a new committee
     */
    public function testCreateCommittee()
    {
        $committee = LDAPI::createCommittee();
        $this->assertTypeCommittee($committee);

        $committee2 = $this->ldapi->createCommitteeWithLink();
        $this->assertTypeCommittee($committee2);
    }

    /**
     * Test deleting a committee
     */
    public function testDeleteCommittee()
    {
        $id = 65000;

        $committee = $this->ldapi->getCommittee($id);

        $this->assertTypeCommittee($committee);

        $result = $this->ldapi->deleteCommittee($committee);

        $this->assertTrue($result);

        $committee = $this->ldapi->getCommittee($id);

        $this->assertFalse($committee);
    }

    /**
     * Test deleting a committee when an alias exists
     */
    public function testDeleteCommitteeAlias()
    {
        $committee = $this->ldapi->getCommitteeByName('kitcat');

        $result = $this->ldapi->deleteCommittee($committee);

        $this->assertTrue($result);

        $alias = $this->ldapi->getCommitteeByName('kitcatalias');

        $this->assertNotContains($committee->name, $alias->member_usernames);
    }

    /**
     * Test saving a new committee
     */
    public function testSaveNewCommittee()
    {
        $data = self::COMMITTEE_DATA;

        $committee = LDAPI::createCommittee($data);

        $success = $this->ldapi->saveNewCommittee($committee);
        
        $this->assertTrue($success);
        
        $new_committee = $this->ldapi->getCommitteeByName($data['name']);
        
        $this->assertTypeCommittee($new_committee);
    }
    
    /**
     * Test trying to create a new committee when the name is already in use
     */
    public function testSaveNewCommitteeExistingName()
    {
        $data = self::COMMITTEE_DATA;
        $data['name'] = 'itco';

        $committee = LDAPI::createCommittee($data);
        
        $success = $this->ldapi->saveNewCommittee($committee);
        
        $this->assertFalse($success);
        
        $errors = $committee->getErrors();
        
        $this->assertArrayHasKey('name', $errors);
    }
    
    /**
     * Test saving a new committee when there is a valiation error
     */
    public function testSaveNewCommitteeValidationError()
    {
        $data = self::COMMITTEE_DATA;
        $data['name'] = '';

        $committee = LDAPI::createCommittee($data);
        
        $success = $this->ldapi->saveNewCommittee($committee);
        
        $this->assertFalse($success);
        
        $errors = $committee->getErrors();
        
        $this->assertArrayHasKey('name', $errors);
    }

}
