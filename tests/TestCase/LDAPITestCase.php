<?php

namespace LDAPI\Test\TestCase;

use LDAPI\LDAPUser;
use PHPUnit\Framework\TestCase;
use LDAPI\LDAPI;
use LDAPI\Test\Fixture\UsersFixture;
use LDAPI\Test\Fixture\CommitteesFixture;
use LDAPI\Test\Fixture\ForwardsFixture;

/**
 * Abstract TestCase extension
 *
 * The main purpose of this TestCase is to set-up and tear-down the test fixtures.
 * Note: a real LDAP instance is required to run these tests!
 */
abstract class LDAPITestCase extends TestCase
{

    /**
     * The API instance used
     *
     * @var LDAPI
     */
    protected $ldapi;

    /**
     * Config as used by LDAPI
     *
     * @var array
     */
    protected $_config;

    /**
     * Fixture to be loaded by ldap
     *
     * @var array
     */
    public $fixtures;

    /**
     * Constructor
     * 
     * Only called once for the entire test suite
     * 
     * @param string|null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $config_path = ROOT . DS . 'config' . DS . 'ldapi.php';
        if (!is_file($config_path))
        {
            trigger_error("Config file `{$config_path}` not found", E_USER_ERROR);
        }

        $configs = include $config_path;

        if (isset($configs['test']))
        {
            $this->_config = $configs['test'];
        }
        else
        {
            trigger_error("Config key `test` does not found in `{$config_path}`!", E_USER_ERROR);
        }
    }

    /**
     * setUp is run before each test
     */
    protected function setUp(): void
    {
        $this->_fillLdap();

        // Load LDAPI through test config
        $this->ldapi = new LDAPI('test');
    }

    /**
     * tearDown is run after each test
     */
    protected function tearDown(): void
    {
        unset($this->ldapi);

        $this->_clearLdap();
    }

    /**
     * Remove all entries in LDAP
     */
    protected function _clearLdap()
    {
        $con = $this->_connectLdap();

        $binds = [
            'people' => '(objectClass=Astatine)',
            'groups' => '(description=com)',
            'forward' => '(sn=forward)'
        ];

        foreach ($binds as $bind => $filter)
        {
            set_error_handler(function() {}, E_WARNING); // Ignore all warnings
            
            $result = ldap_search(
                    $con,
                    $this->_config['bind'][$bind],
                    $filter,
                    ['cn']
            );
            
            restore_error_handler();
            
            if (!$result)
            {
                continue; // No objects found or bind doesn't really exist
            }

            $entries = ldap_get_entries($con, $result);

            $this->_removeEntries($con, $entries);
        }
    }

    /**
     * Connect and bind to the ldap
     * 
     * @return resource LDAP link identifier
     */
    protected function _connectLdap()
    {
        $connection = ldap_connect($this->_config['host']);

        if (!$connection)
        {
            trigger_error('Failed to connect to LDAP, check your config', E_USER_ERROR);
        }

        ldap_set_option($connection, LDAP_OPT_PROTOCOL_VERSION, 3);

        $bind = ldap_bind($connection,
                $this->_config['bind']['admin'],
                $this->_config['admin_password']
        );

        if (!$bind)
        {
            trigger_error('Failed to bind to LDAP, check your credentials', E_USER_ERROR);
        }

        return $connection;
    }

    /**
     * Remove entries return from `ldap_get_entries`
     * 
     * @param resource
     * @param array $entries
     */
    protected function _removeEntries($con, $entries)
    {
        $success = true;

        if (count($entries) > 10)
        {
            trigger_error('More than 10 items to be removed, are you really on the test database?', E_USER_ERROR);
        }

        foreach ($entries as $key => $entry)
        {
            if (!is_numeric($key))
            {
                continue;
            }

            $dn = $entry['dn'];

            if (!ldap_delete($con, $dn))
            {
                $success = false;
                echo "Failed to delete {$dn}\n";
            }
        }
    }

    /**
     * Run LDAP fixtures
     */
    protected function _fillLdap()
    {
        $con = $this->_connectLdap();

        foreach ($this->fixtures as $fixture)
        {
            switch ($fixture)
            {
                case 'Users':
                    $field = 'uid';
                    $bind = $this->_config['bind']['people'];
                    $entries = UsersFixture::getData();
                    break;
                case 'Committees':
                    $field = 'cn';
                    $bind = $this->_config['bind']['groups'];
                    $entries = CommitteesFixture::getData();
                    break;
                case 'Forwards':
                    $field = 'cn';
                    $bind = $this->_config['bind']['forward'];
                    $entries = ForwardsFixture::getData();
                    break;
                default:
                    trigger_error("Fixture `{$fixture}` not defined!", E_USER_ERROR);
            }

            foreach ($entries as $entry)
            {
                set_error_handler(function() {}, E_WARNING); // Disable warnings
                
                $rdn = $field . '=' . $entry[$field] . ',' . $bind;
                
                $success = ldap_add(
                        $con,
                        $rdn,
                        $entry
                );
                
                restore_error_handler();

                if (!$success)
                {
                    echo "Failed to add `{$rdn}`\n";
                }
            }
        }
    }

    /**
     * Assert the object is an instance of LDAPUser
     *
     * @param mixed $data
     * @param string $message
     */
    public function assertTypeUser($data, $message = '')
    {
        $this->assertInstanceOf(LDAPUser::class, $data, $message);
    }

    /**
     * Assert the object is an instance of LDAPCommittee
     * 
     * @param mixed $data
     * @param string $message
     */
    public function assertTypeCommittee($data, $message = '')
    {
        $this->assertInstanceOf('\LDAPI\LDAPCommittee', $data, $message);
    }

    /**
     * Assert array is empty
     * 
     * @param mixed $data
     * @param string $message
     */
    public function assertArrayEmpty($data, $message = 'Array is not empty')
    {
        $this->assertTrue(empty($data), $message);
    }

    /**
     * Assert array is not empty
     * 
     * @param mixed $data
     * @param string $message
     */
    public function assertArrayNotEmpty($data, $message = 'Array is empty')
    {
        $this->assertFalse(empty($data), $message);
    }

    /**
     * Assert object has the key-value pairs of a provided array
     * 
     * @param object $object
     * @param array $array
     * @param string $message
     */
    public function assertObjectMatchesArray($object, $array, $message = 'Array is not exact subset of given object')
    {
        foreach ($array as $key => $value)
        {
            $this->assertTrue(isset($object->{$key}), "Failed to assert the object has attribute `{$key}`");
            $this->assertSame($value, $object->{$key}, $message);
        }
    }
    
    /**
     * Assert array has a certain subset (only top level can be subset)
     * 
     * For non-numeric keys, the keys are matched as well
     * 
     * @param array $subset
     * @param array $array
     * @param string $message
     */
    public function assertArrayMatchesArray($subset, $array, $message = 'Array is not a subset of the expected')
    {
        foreach ($subset as $key => $value)
        {
            if (is_numeric($key))
            {
                $this->assertTrue(in_array($value, $array), $message);
            }
            else
            {
                $this->assertArrayHasKey($key, $array, $message);
                $this->assertSame($value, $array[$key], $message);
            }
        }
    }

}
