<?php

namespace LDAPI\Test\TestCase;

/**
 * Test LDAPI general features
 */
class LDAPIGeneralTest extends LDAPITestCase
{

    public $fixtures = ['Users', 'Committees'];

    /**
     * Test the records limit method
     */
    public function testSetLimit()
    {
        $this->ldapi->setLimit(2);
        $users_limited = $this->ldapi->findAllUsers();
        $this->assertCount(2, $users_limited);

        $this->ldapi->setLimit(false);
        $users = $this->ldapi->findAllUsers();
        $this->assertGreaterThan(3, count($users));
    }

    /**
     * Test the pagination feature of the LDAPI
     */
    public function testPaginate()
    {
        $options = [
            'sort' => 'lastname',
            'limit' => 2,
            'page' => 1
        ];

        $users = $this->ldapi->findAllUsers();
        $this->assertGreaterThan(3, count($users));

        $page_count = $this->ldapi->paginate($users, $options);

        $this->assertGreaterThan(1, $page_count);
        $this->assertCount($options['limit'], $users);

        $options['page'] = 2;

        $users2 = $this->ldapi->findAllUsers();
        $this->ldapi->paginate($users2, $options);

        $overlap = false;
        foreach ($users as $user)
        {
            foreach ($users2 as $user2)
            {
                if ($user->id == $user2->id)
                {
                    $overlap = true;
                    break;
                }
            }
        }
        $this->assertFalse($overlap); // Really was paginated
    }

}