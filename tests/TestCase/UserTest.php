<?php

namespace LDAPI\Test\TestCase;

use LDAPI\LDAPUser;

/**
 * Test the LDAPIUser class
 */
class UserTest extends LDAPITestCase
{

    public $fixtures = [
        'Users',
        'Committees',
        'Forwards'
    ];

    const USER_ID = 3000;

    /**
     * Test changing a user password
     */
    public function testChangePassword()
    {
        $user = $this->ldapi->getUser(self::USER_ID);

        // Change password without checking
        $password = 'herpaderp';

        $success = $user->changePassword($password);

        $this->assertTrue($success);

        // Test change
        $user2 = $this->ldapi->login($user->username, $password);

        $this->assertTypeUser($user2);

        // Change password again, now requiring verification
        $password2 = 'supercoolpassword';

        $success2 = $user2->changePassword($password2, $password);

        $this->assertTrue($success2);

        $user3 = $this->ldapi->login($user2->username, $password2);

        $this->assertTypeUser($user3);
    }

    /**
     * Test trying to change a user password with an error
     */
    public function testChangePasswordError()
    {
        $user = $this->ldapi->getUser(self::USER_ID);

        $passwords = [
            'short' => null,
            '1234' => null,
            'strongpassphrase' => 'invalid_password'
        ];

        foreach ($passwords as $password => $current_password)
        {
            $success = $user->changePassword($password, $current_password);

            $this->assertFalse($success);

            $this->assertArrayHasKey('password', $user->getErrors());
        }
    }

    /**
     * Test committees retrieval for a user
     */
    public function testCollectCommittees()
    {
        $user = $this->ldapi->getUser(self::USER_ID);

        $this->assertNull($user->committees);

        $user->collectCommittees();

        $this->assertArrayNotEmpty($user->committees);

        foreach ($user->committees as $committee)
        {
            $this->assertTypeCommittee($committee);
        }
    }

    /**
     * Test the getAstatineMail method
     */
    public function testGetAstatineEmail()
    {
        $user = $this->ldapi->getUser(self::USER_ID);

        $mail = $user->getAstatineEmail();

        $this->assertSame($user->username . '@astatine.utwente.nl', $mail);
    }

    /**
     * Test the getAuthorization method
     */
    public function testGetAuthorization()
    {
        $user = $this->ldapi->getUser(self::USER_ID);

        $auth = $user->getAuthorization();

        $this->assertTrue(is_string($auth));
        $this->assertFalse(is_numeric($auth));
    }

    /**
     * Test getBirthdate method
     */
    public function testGetBirthdate()
    {
        $user = $this->ldapi->getUser(self::USER_ID);

        $date = $user->getBirthdate();

        $this->assertInstanceOf('DateTime', $date);
    }

    /**
     * Test getFormalName method
     */
    public function testGetFormalName()
    {
        $user = $this->ldapi->getUser(self::USER_ID);

        $name = $user->getFormalname();

        $this->assertSame('P. Tester', $name);
    }

    /**
     * Test getFullname() method
     */
    public function testGetFullname()
    {
        $user = $this->ldapi->getUser(self::USER_ID);

        $name = $user->getFullname();

        $this->assertSame('Piet Tester', $name);
    }

    /**
     * Test getJoinDate method
     */
    public function testGetJoinDate()
    {
        $user = $this->ldapi->getUser(self::USER_ID);

        $date = $user->getJoindate();

        $this->assertInstanceOf('DateTime', $date);
    }

    /**
     * Test getMembership method
     */
    public function testGetMembership()
    {
        $user = $this->ldapi->getUser(self::USER_ID);

        $type = $user->getMembership();

        $this->assertTrue(is_string($type));
        $this->assertFalse(is_numeric($type));
    }

    /**
     * Test getClassYear method
     */
    public function testGetClassYear()
    {
        $user = $this->ldapi->getUser(self::USER_ID);

        $years = $user->getClassYear();

        $this->assertSame('2004/2005', $years);
    }

    /**
     * Test change in username
     */
    public function testSaveChangeUsername()
    {
        $user = $this->ldapi->getUser(self::USER_ID);

        $committees = $this->ldapi->findCommitteesByValues([
            'memberuid' => $user->username
        ]);

        $list = array_map(function ($object)
        {
            return $object->name;
        }, $committees);

        $username = 'piet.tester3000';

        $success = $user->saveChangeUsername($username);

        $this->assertTrue($success);

        $user_after = $this->ldapi->getUserByUsername($username);

        $this->assertNotFalse($user_after);
        $this->assertSame($user->id, $user_after->id);

        $committees_after = $this->ldapi->findCommitteesByValues([
            'memberuid' => $user_after->username
        ]);

        $list_after = array_map(function ($object)
        {
            return $object->name;
        }, $committees_after);

        $this->assertSame($list, $list_after);
    }

    /**
     * Test change in username with error
     */
    public function testSaveChangeUsernameError()
    {
        $user = $this->ldapi->getUser(self::USER_ID);

        $username = 'robert.roos';

        $success = $user->saveChangeUsername($username);

        $this->assertFalse($success);

        $this->assertArrayHasKey('username', $user->getErrors());
    }

    /**
     * Test email-forward retrieval
     */
    public function testGetEmailForward()
    {
        $user = $this->ldapi->getUser(self::USER_ID);

        $forward = $user->getEmailForward();

        $this->assertNotNull($forward);
        $this->assertSame('piet.tester@gmail.com', $forward);
    }

    /**
     * Test saving a new email-forward (overwriting the old one)
     */
    public function testSaveEmailForward()
    {
        $user = $this->ldapi->getUser(self::USER_ID);

        $email = 'piet@live.nl';

        $success = $user->saveEmailForward($email);

        $this->assertTrue($success);

        $this->assertSame($email, $user->getEmailForward());
    }

    /**
     * Test saving a new email-forward when none existed yet
     */
    public function testSaveEmailForwardNew()
    {
        $user = $this->ldapi->getUserByUsername('robert.roos');

        $email = 'bobb.ross@gmail.com';

        $success = $user->saveEmailForward($email);

        $this->assertTrue($success);

        $this->assertSame($email, $user->getEmailForward());
    }
    
    /**
     * Test trying to save a new email-forward with error
     */
    public function testSaveEmailForwardError()
    {
        $user = $this->ldapi->getUser(self::USER_ID);

        $email = 'invalid';

        $success = $user->saveEmailForward($email);

        $this->assertFalse($success);

        $this->assertArrayHasKey('emailforward', $user->getErrors());
    }

    /**
     * Test magic debug method
     */
    public function testDebug()
    {
        $user = $this->ldapi->getUser(self::USER_ID);

        $debug = $user->__debugInfo();
        $this->assertArrayHasKey('username', $debug);
    }

    /**
     * Test static method for current class
     */
    public function testCurrentClass()
    {
        $first = strtotime('01-07-2004');
        $now = time();

        // Get difference in years between timestamps
        $diff = floor(($now - $first) / 60 / 60 / 24 / 365);

        $class = LDAPUser::getCurrentClass();
        $this->assertEquals($diff + 1, $class);
    }

}
