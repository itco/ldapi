<?php

namespace LDAPI\Test\TestCase;

/**
 * Test the LDAPICommittee object
 */
class CommitteeTest extends LDAPITestCase
{

    public $fixtures = [
        'Users',
        'Committees'
    ];

    const COMMITTEE_ID = 65000;

    /**
     * Test adding members to a committee
     */
    public function testAddMembers()
    {
        $committee = $this->ldapi->getCommittee(self::COMMITTEE_ID);

        $new_members = ['piet2.tester', 'bob.marly'];

        $success = $committee->addMembers($new_members);

        $this->assertTrue($success);

        $committee_after = $this->ldapi->getCommittee(self::COMMITTEE_ID);

        $this->assertArrayMatchesArray(
                $new_members,
                $committee_after->member_usernames
        );
    }
    
    /**
     * Test adding a single string as member to a committee
     */
    public function testAddMembersSingle()
    {
        $committee = $this->ldapi->getCommittee(self::COMMITTEE_ID);

        $new_member = 'bob.marly';

        $success = $committee->addMembers($new_member);

        $this->assertTrue($success);

        $committee_after = $this->ldapi->getCommittee(self::COMMITTEE_ID);

        $this->assertArrayMatchesArray(
                [$new_member],
                $committee_after->member_usernames
        );
    }

    /**
     * Test adding a committee to another committee
     */
    public function testAddMembersAlias()
    {
        $committee = $this->ldapi->getCommittee(self::COMMITTEE_ID);

        $new_members = ['kitcatalias']; // Try to add a committee to a committee

        $success = $committee->addMembers($new_members);

        $this->assertTrue($success);

        $committee_after = $this->ldapi->getCommittee(self::COMMITTEE_ID);

        $this->assertArrayMatchesArray(
            $new_members,
            $committee_after->member_usernames
        );
    }

    /**
     * Test trying to add a committee to another committee, which already contains the first committee
     */
    public function testAddMembersAliasCircular()
    {
        $committee = $this->ldapi->getCommitteeByName('kitcat');

        $new_members = ['kitcatalias']; // But 'kitcat' is already a member of 'kitcatalias'

        $success = $committee->addMembers($new_members);

        $this->assertFalse($success);

        $errors = $committee->getErrors();
        $this->assertArrayHasKey('members', $errors);
        $this->assertArrayHasKey('kitcatalias', $errors['members'][0]);

        $committee_after = $this->ldapi->getCommittee($committee->id);

        $this->assertNotContains($new_members[0], $committee_after->member_usernames);
    }

    /**
     * Test trying to add a committee to another committee, while that committee already has a circular problem
     */
    public function testAddMembersAliasCircularProblem()
    {
        $committee = $this->ldapi->getCommittee(self::COMMITTEE_ID);

        $new_members = ['circularalias1']; // But 'circularalias1' is in a circular conflict with 'circularalias2'

        $success = $committee->addMembers($new_members);

        $this->assertFalse($success);
        $this->assertArrayNotEmpty($committee->getErrors());
    }
    
    /**
     * Test adding a member to a committee as object
     */
    public function testAddMembersObject()
    {
        $committee = $this->ldapi->getCommittee(self::COMMITTEE_ID);

        $user = $this->ldapi->getUserByUsername('bob.marly');

        $success = $committee->addMembers($user);

        $this->assertTrue($success);

        $committee_after = $this->ldapi->getCommittee(self::COMMITTEE_ID);

        $this->assertArrayMatchesArray(
                [$user->username],
                $committee_after->member_usernames
        );
    }
    
    /**
     * Test the retrieval of members of a committee
     */
    public function testCollectMembers()
    {
        $committee = $this->ldapi->getCommittee(self::COMMITTEE_ID);
        
        $committee->collectMembers();
        
        $this->assertArrayNotEmpty($committee->members);
        
        foreach ($committee->members as $member)
        {
            $this->assertTypeUser($member);
        }
    }
    
    /**
     * Test the retrieval of members of a committee when one of the members cannot be found
     */
    public function testCollectMembersNonExistent()
    {
        $committee = $this->ldapi->getCommitteeByName('kitcat');
        
        $committee->collectMembers();
        
        $this->assertArrayNotEmpty($committee->members);
        
        foreach ($committee->members as $member)
        {
            $this->assertTypeUser($member);
            $this->assertNotEquals('non.existent', $member->username);
        }
    }
    
    /**
     * Test removing members from a committee as a list of usernames
     */
    public function testRemoveMembers()
    {
        $committee = $this->ldapi->getCommittee(self::COMMITTEE_ID);

        $old_members = ['piet2.tester', 'bob.marly'];

        $success = $committee->removeMembers($old_members);

        $this->assertTrue($success);

        $committee_after = $this->ldapi->getCommittee(self::COMMITTEE_ID);

        foreach ($committee_after->member_usernames as $uid)
        {
            $this->assertFalse(in_array($uid, $old_members));
        }
    }
    
    /**
     * Test saving a committee rename
     */
    public function testSaveRename()
    {
        $committee = $this->ldapi->getCommittee(self::COMMITTEE_ID); 
        
        $name = 'awesomeitco';
        
        $success = $committee->saveRename($name);
        
        $this->assertTrue($success);
        
        $committee_after = $this->ldapi->getCommitteeByName($name);
        
        $this->assertSame($committee->id, $committee_after->id);
    }
    
    /**
     * Test saving a committee rename when that committee already exists
     */
    public function testSaveRenameError()
    {
        $committee = $this->ldapi->getCommittee(self::COMMITTEE_ID); 
        
        $name = 'board';
        
        $success = $committee->saveRename($name);
        
        $this->assertFalse($success);
        
        $this->assertArrayHasKey('name', $committee->getErrors());
    }

    /**
     * Test magic debug method
     */
    public function testDebug()
    {
        $committee = $this->ldapi->getCommittee(self::COMMITTEE_ID);

        $debug = $committee->__debugInfo();
        $this->assertArrayHasKey('members', $debug);
    }
}
