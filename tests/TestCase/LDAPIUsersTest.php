<?php

namespace LDAPI\Test\TestCase;

use LDAPI\LDAPI;

/**
 * Test the User methods of the LDAPI class
 */
class LDAPIUsersTest extends LDAPITestCase
{

    const USER_DATA = [
        'address' => 'Groove Street 69',
        'attentie' => 0,
        'authorization' => 4,
        'bic' => 'INGBNL2A',
        'birthdate' => '1990-01-01',
        'city' => 'Vice City',
        'class' => 15,
        'country' => 'nl',
        'email' => 'voornaam.achternaam50@gmail.com',
        'emailaliases' => ['voornaam.achternaam'],
        'firstname' => 'Voornaam',
        'iban' => 'NL57DNIB0400333821',
        //'id' => null,
        'initials' => 'V.',
        'insertions' => null,
        'joindate' => '2019-10-11',
        'lastname' => 'Achternaam',
        'membership' => 0,
        'names' => 'Voornaam',
        'nationality' => 'Dutch',
		'personalemail' => 'persoonlijk.emailadres50@gmail.com',
        'phonenumber' => '0612345678',
        'phonenumber_home' => '0201234567',
        'postalcode' => '6969AA',
        'studentnumber' => '1599999',
        'username' => 'voornaam.achternaam'
        //'password' => 'mijnwachtwoord'
    ];

    public $fixtures = [
        'Users',
        'Forwards'
    ];

    /**
     * Test getting a singly user by id
     */
    public function testGetUser()
    {
        $id = 3000;

        $user = $this->ldapi->getUser($id);

        $this->assertTypeUser($user);
        $this->assertEquals($id, $user->id);
        $this->assertEquals('piet.tester', $user->username);
    }

    /**
     * Test getting a single user when it could not be found
     */
    public function testGetUserNotFound()
    {
        $id = 9999;

        $user = $this->ldapi->getUser($id);

        $this->assertFalse($user);
    }

    /**
     * Test getting multiple users by id
     */
    public function testGetUsers()
    {
        $users_empty = $this->ldapi->getUsers([]);
        $this->assertArrayEmpty($users_empty);

        $ids = [3000, 3001];

        $users = $this->ldapi->getUsers($ids);

        $this->assertArrayNotEmpty($users);

        foreach ($users as $id => $user)
        {
            $this->assertTypeUser($user);
            $this->assertTrue(in_array($user->id, $ids));
            $this->assertEquals($id, $user->id);
        }
    }

    /**
     * Test getting multiple users by id when at least one was not found
     */
    public function testGetUsersNotFound()
    {
        $ids = [3000, 9999];

        $users = $this->ldapi->getUsers($ids);

        $this->assertArrayNotEmpty($users);

        foreach ($users as $id => $user)
        {
            $this->assertTypeUser($user);
            $this->assertTrue(in_array($user->id, $ids));
            $this->assertNotEquals(9999, $user->id);
            $this->assertEquals($id, $user->id);
        }
    }

    /**
     * Test getting a user by their username
     */
    public function testGetUserByUsername()
    {
        $username = 'piet.tester';

        $user = $this->ldapi->getUserByUsername($username);

        $this->assertTypeUser($user);
        $this->assertEquals($username, $user->username);
    }

    /**
     * Test getting a user by their studentnumber
     */
    public function testGetUserByStudentnumber()
    {
        $number = '1234567';

        $user = $this->ldapi->getUserByStudentnumber($number);

        $this->assertTypeUser($user);
        $this->assertEquals($number, $user->studentnumber);

        $user_none = $this->ldapi->getUserByStudentnumber('6519644');
        $this->assertFalse($user_none);
    }

    /**
     * Test user authentication
     */
    public function testLogin()
    {
        $username = 'piet.tester';
        $password = 'wachtwoord';

        $user = $this->ldapi->login($username, $password);

        $this->assertTypeUser($user);
        $this->assertEquals($username, $user->username);
    }

    /**
     * Test incorrect user authentication
     */
    public function testLoginIncorrect()
    {
        $username = 'piet.tester';
        $password = 'herpaderp';

        $user = $this->ldapi->login($username, $password);

        $this->assertFalse($user);
    }

    /**
     * Test searching for users
     */
    public function testFindUsersByValues()
    {
        $lastname = 'Tester';
        $users = $this->ldapi->findUsersByValues(['lastname' => $lastname]);

        $this->assertArrayNotEmpty($users);

        foreach ($users as $id => $user)
        {
            $this->assertTypeUser($user);
            $this->assertEquals($lastname, $user->lastname);
            $this->assertEquals($id, $user->id);
        }
    }

    /**
     * Test searching for users with no results
     */
    public function testFindUsersByValuesEmpty()
    {
        $users = $this->ldapi->findUsersByValues(['firstname' => 'i_dont_exist']);

        $this->assertArrayEmpty($users);
    }

    /**
     * Test retrieving all users
     */
    public function testFindAllUsers()
    {
        $users = $this->ldapi->findAllUsers();

        $this->assertArrayNotEmpty($users);

        foreach ($users as $id => $user)
        {
            $this->assertTypeUser($user);
            $this->assertEquals($id, $user->id);
        }
    }

    /**
     * Test creating a new user
     */
    public function testCreateUser()
    {
        $user = LDAPI::createUser();
        $this->assertTypeUser($user);

        $user2 = $this->ldapi->createUserWithLink();
        $this->assertTypeUser($user2);
    }

    /**
     * Test deleting a user
     */
    public function testDeleteUser()
    {
        $id = 3000;

        $user = $this->ldapi->getUser($id);

        $forward = $user->getEmailForward();
        $this->assertNotFalse($forward);

        $this->assertTypeUser($user);

        $result = $this->ldapi->deleteUser($user);
        $this->assertTrue($result);

        $user2 = $this->ldapi->getUser($id);
        $this->assertFalse($user2);

        $forward2 = $user->getEmailForward();
        $this->assertFalse($forward2); // Make sure forward got deleted too
    }

    /**
     * Test saving a new user
     */
    public function testSaveNewUser()
    {
        $data = self::USER_DATA;
        $username = $data['username'];
        $password = 'mijnwachtwoord';

        $this->assertFalse(
            $this->ldapi->login($username, $password)
        );

        $user = LDAPI::createUser($data);

        $this->assertNull($user->id);

        $success = $this->ldapi->saveNewUser($user, $password);

        $this->assertNotNull($user->id); // Confirm the id has been placed in the original object

        $this->assertTrue($success);

        $user_saved = $this->ldapi->getUserByUsername($username);
        $this->assertTypeUser($user_saved);
        $this->assertGreaterThan(3003, $user_saved->id); // Check the id has been chosen correctly
    }

    /**
     * Test saving a new user when the username is taken
     */
    public function testSaveNewUserExistingUsername()
    {
        $data = self::USER_DATA;
        $data['username'] = 'piet.tester';

        $user = LDAPI::createUser($data);

        $success = $this->ldapi->saveNewUser($user, 'mijnwachtwoord');

        $this->assertFalse($success);

        $errors = $user->getErrors();

        $this->assertArrayHasKey('username', $errors);
    }

    /**
     * Test trying to save a new user with validation errors
     */
    public function testSaveNewUserValidationError()
    {
        $data = self::USER_DATA;
        $data['email'] = 'invalid.com';

        $user = LDAPI::createUser($data);

        $success = $this->ldapi->saveNewUser($user, 'mijnwachtwoord');

        $this->assertFalse($success);

        $errors = $user->getErrors();

        $this->assertArrayHasKey('email', $errors);
    }

    /**
     * Test saving a modified user
     */
    public function testSaveUpdatedUser()
    {
        $id = 3000;

        $user = $this->ldapi->getUser($id);

        $data = [
            'firstname' => 'Pietjes',
            'postalcode' => '1111AA'
        ];

        $user->patchEntity($data);

        $success = $this->ldapi->saveUpdatedUser($user);

        $this->assertTrue($success);

        $user2 = $this->ldapi->getUser($id);

        $this->assertObjectMatchesArray($user2, $data);
    }

    /**
     * Test trying to save a modified user with validation errors
     */
    public function testSaveUpdatedUserValidationError()
    {
        $id = 3000;

        $user = $this->ldapi->getUser($id);

        $data = [
            'email' => 'invalid.example.com'
        ];

        $user->patchEntity($data);

        $success = $this->ldapi->saveUpdatedUser($user);

        $this->assertFalse($success);

        $errors = $user->getErrors();
        $this->assertArrayHasKey('email', $errors);

        $user2 = $this->ldapi->getUser($id);

        $this->assertNotEquals($data['email'], $user2->email);
    }

    /**
     * Test behaviour of null and empty ("") fields
     */
    public function testPatchNull()
    {
        $user = $this->ldapi->getUserByUsername('piet.tester');

        $datas = [
            ['value' => 'van', 'check' => 'van'],
            ['value' => '', 'check' => null],
            ['value' => 'van', 'check' => 'van'],
            ['value' => null, 'check' => null],
            ['value' => 'van', 'check' => 'van'],
            ['value' => [], 'check' => null],
        ];

        foreach ($datas as $data)
        {
            $user->patchEntity(['insertions' => $data['value']]);
            $result = $this->ldapi->saveUpdatedUser($user);

            $this->assertNotFalse($result, 'Failed to save user');

            $user2 = $this->ldapi->getUser($user->id);
            $this->assertEquals($data['check'], $user2->insertions, 'Unexpected result');
        }
    }

}
