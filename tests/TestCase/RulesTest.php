<?php

namespace LDAPI\Test\TestCase;

use PHPUnit\Framework\TestCase;
use LDAPI\Rules;

/**
 * Test the static validation rules provider
 */
class RulesTest extends TestCase
{

    /**
     * Assert that $result is `true` when $expected == true, or a string
     *
     * @param $expected
     * @param $result
     */
    protected function assertRulesResult($expected, $result)
    {
        if ($expected)
        {
            $this->assertSame(true, $result);
        }
        else
        {
            $this->assertTrue(is_string($result));
        }
    }

    /**
     * Test studentnumber validation
     */
    public function testIsStudentnumber()
    {
        $numbers = [
            '7661219' => true,
            '1111111' => true,
            '76612199' => false, // Too long
            's7661219' => false, // Starts with 's'
            '76464-5' => false, // Contains special character
            '766121a' => false, // Contains letter
        ];

        foreach ($numbers as $number => $valid)
        {
            $result = Rules::is_studentnumber($number);
            $this->assertRulesResult($valid, $result);
        }
    }

    /**
     * Test valid date parser
     */
    public function testValidDate()
    {
        $dates = [
            '1795-1-1' => false,
            '2030-1-1' => false,
            '2019/2/3' => false,
            '2020-35-1' => false,
            '2020--1-20' => false,
            '2020--1--1' => false,
            '1996-12-2' => true,
            '1996-12-02' => true,
        ];

        foreach ($dates as $date => $valid)
        {
            $result = Rules::is_valid_date($date);
            $this->assertRulesResult($valid, $result);
        }
    }

    /**
     * Test IBAN validation
     */
    public function testValidIban()
    {
        $ibans = [
            'NL73ABNA8253182457' => true,
            'CR4303298374132619956' => true,
            'NL73ABNA8253182456' => false,
            'CR430329837413261995' => false,
            'yololollolol' => false,
        ];

        foreach ($ibans as $iban => $valid)
        {
            $result = Rules::valid_iban($iban);
            $this->assertRulesResult($valid, $result);
        }
    }

}