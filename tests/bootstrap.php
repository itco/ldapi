<?php

if (is_file('vendor/autoload.php'))
{
    require_once 'vendor/autoload.php';
}
else
{
    require_once dirname(__DIR__) . '/vendor/autoload.php';
}

require_once 'dump.php';

if (!defined('DS'))
{
    define('DS', DIRECTORY_SEPARATOR);
}

// Set the parent of the 'tests' directory as root
define('ROOT', dirname(__DIR__));
