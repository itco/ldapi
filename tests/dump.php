<?php

/**
 * Display debug info
 *
 * This function should mimic CakePHP's debug(), since we like but it is not available here.
 *
 * @param mixed $data Data to be displayed
 */
function dump($data)
{
    $bt = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, 1);
    $caller = array_shift($bt);

    $file = $caller['file'] . ':' . $caller['line'];

    if (is_array($data))
    { //If the given variable is an array, print using the print_r function.
        print "------ {$file}\n";
        print_r($data);
        print "-----------------------\n";
    }
    elseif (is_object($data))
    {
        print "===== {$file}\n";
        var_dump($data);
        print "===========================\n";
    }
    else
    {
        print "===== {$file}\n";
        var_dump($data);
        print "=========\n";
    }
}