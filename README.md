# LDAPI

[![CodeFactor](https://www.codefactor.io/repository/bitbucket/itco/ldapi/badge)](https://www.codefactor.io/repository/bitbucket/itco/ldapi)
[![codecov](https://codecov.io/bb/itco/ldapi/branch/master/graph/badge.svg)](https://codecov.io/bb/itco/ldapi)
[![Documentation Status](https://readthedocs.org/projects/ldapi/badge/?version=latest)](https://ldapi.readthedocs.io/en/latest/?badge=latest)


This repository contains the source of the LDAPI.  
Use composer to get an instance of this library. Clone the repository directly if you plan on developing it further.

Consider the corresponding BitBucket wiki for more detailed information and instructions: https://bitbucket.org/itco/ldapi/wiki/Home

## Documentation

Documentation is automatically built and hosted by ReadTheDocs: <https://ldapi.readthedocs.io/>

This documentation is built through Doxygen and Sphinx (Python package), with the Breathe extension. Build it like:

```shell
cd docs
make html
```

Run `pip install -r docs/requirements.txt` to get the required packages.  
The build output can be found in `docs/_build/html`.

### PHPDoc ###

An easier way to build documentation is using [phpDocumentor](https://www.phpdoc.org/). Generated PHPDoc documentation can be found here: https://itco.bitbucket.io/ldapi/index.html (note: this is *not* build automatically and can be out-of-date).

Generate documentation with:

```shell
phpdoc -d ./src -t ./phpdoc_build
```

Use `-d` to set the input and `-t` to set the output.

## Acquire with Composer

 * Run `composer require itco/ldapi`. The package is added to the list of packages that should be downloaded.
 * Run `composer update` to actually download them.
 * Create `config/ldapi.php` with your configuration. `vendor/itco/ldapi/cofnig/ldapi.default.php` is available as a reference.

Take a look at the wiki for more on information on deployment, usage and development.

## Dev

Require `itco/ldapi:dev-development` to also get unstable latest versions.

## Changelog

See [CHANGELOG.md](CHANGELOG.md) for an overview.
