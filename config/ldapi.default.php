<?php

/**
 * This file contains the default configuration, containing the keys needed for
 * a real config file. When deployed into an application, this file should be
 * copied to `config/ldapi.php` and `config/ldapi.default.php`. The first should
 * be modified and excluded from commits to prevent online passwords!
 */
return [
    'default' => [
        'host' => 'ldap://example.nl:389',
        'admin_password' => 'secret',
        'bind' => [
            'admin' => 'cn=admin,dc=example,dc=nl',
            'people' => 'ou=People,dc=example,dc=nl',
            'forward' => 'ou=forward,dc=example,dc=nl',
            'groups' => 'ou=Group,dc=example,dc=nl',
        ],
        'write' => false,
        'write_password' => ''
    ],
    'test' => [
        'host' => 'ldap://localhost',
        'admin_password' => 'admin',
        'bind' => [
            'admin' => 'cn=admin,dc=astatine,dc=example,dc=org',
            'people' => 'ou=People,dc=astatine,dc=example,dc=org',
            'forward' => 'ou=forward,dc=astatine,dc=example,dc=org',
            'groups' => 'ou=Group,dc=astatine,dc=example,dc=org',
        ],
        'write' => true,
        'write_password' => 'i_understand_the_dangers_of_editing_memberdata'
    ]
];
