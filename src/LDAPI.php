<?php

/**
 * This file contains the LDAPI class
 */

namespace LDAPI;

/**
 * LDAP Connector class
 *
 * This is the main class to interact with the LDAP. Avoid using functions
 * starting with an underscore ("_") directly, that should not be necessary.
 *
 * @package     LDAPI
 * @author      ITco <itco@astatine.utwente.nl>
 * @author      Robert Roos <robert.roos@astatine.utwente.nl>
 * @copyright   September 2017 - 2018, S.A. Astatine
 * @license     PHP License 3.01
 */
class LDAPI
{

    /**
     * Configuration from config file
     *
     * @var array|null
     */
    protected $config = null;

    /**
     * Bind paths of the Astatine LDAP
     *
     * @var array|null
     */
    protected $bind = null;

    /**
     * The LDAP connection used by default
     *
     * @var resource|null
     */
    protected $connection = null;

    /**
     * Write protected, enabled with valid in config
     *
     * @var bool
     */
    protected $write_enable = false;

    /**
     * Initialize class, create a new LDAP connection using the config set
     *
     * @param string $config_key
     * @return void
     */
    public function __construct($config_key = 'default')
    {
        $this->config = $this->_getConfig($config_key);

        $this->bind = $this->config['bind']; // Array, (people, admin, groups, forward)

        $this->connection = $this->_makeConnection();

        // Do an admin bind, so all data can be retrieved
        ldap_bind($this->connection, $this->bind['admin'], $this->config['admin_password']);

        // Check for write mode
        if ($this->config['write'] === true)
        {
            if ($this->config['write_password'] === 'i_understand_the_dangers_of_editing_memberdata')
                $this->write_enable = true;
            else
                trigger_error('LDAPI Error: invalid write password. Disable write mode or use correct key - ', E_USER_ERROR);
        }
    }

    /**
     * Neatly close the LDAP connection
     */
    public function __destruct()
    {
        $this->_close();
    }

    /**
     * Get config from file
     *
     * @param string $config_key Section of config to use
     * @return array
     */
    protected function _getConfig($config_key)
    {
        if (!defined('DS'))
        {
            define('DS', DIRECTORY_SEPARATOR);
        }

        if (defined('ROOT'))
        {
            $config_path = ROOT . DS;
        }
        else
        {
            $config_path = dirname(__DIR__) . DS;
        }

        $config_path .= 'config' . DS . 'ldapi.php';

        if (file_exists($config_path) == false)
        {
            trigger_error('LDAPI config file missing, make sure the file "ROOT/config/ldapi.php" exists and can be read', E_USER_ERROR);
        }

        $configs = include $config_path;

        if (isset($configs[$config_key]))
        {
            return $configs[$config_key];
        }

        trigger_error("Config with key `{$config_key}` does not exist in `{$config_path}`!", E_USER_ERROR);
    }

    /**
     * Attempt to login, return user on success
     *
     * @param string $username
     * @param string $password (clear, un-hashed)
     * @return LDAPUser|false
     */
    public function login($username, $password)
    {
        $bind = "uid=$username," . $this->bind[LDAPUser::BIND_KEY];

        // Note the admin bind is not used here.
        // Instead, a separate connection is used so the admin bind remains
        // unaffected by this user bind.
        $login_connection = $this->_makeConnection();

        set_error_handler([$this, '_ldap_warning_handler'], E_WARNING); // Catch LDAP warning

        if (ldap_bind($login_connection, $bind, $password))
        {
            restore_error_handler();

            // Login is valid
            $query = ldap_search($login_connection, $bind, LDAPUser::SEARCH_PREFIX, LDAPUser::_getLdapKey());
            $entry = ldap_get_entries($login_connection, $query)[0]; // First entry

            $user = new LDAPUser($entry, $this);

            // Close this connection
            ldap_close($login_connection);

            return $user;
        }
        else
        {
            restore_error_handler();

            // Login is not valid
            return false;
        }
    }

    /**
     * Find objects based on field(s)
     *
     * @param string|LDAPObject $class    LDAPI class to search for
     * @param array             $criteria Associated array of search criteria, use
     *                                    second level for "or", null for returning
     *                                    all objects
     * @param bool              $by_id    Set to true to create array based on ID
     * @return LDAPObject[]|false   Array of LDAP objects
     */
    protected function _findObjectsByValues($class, $criteria = [], $by_id = false)
    {
        // Build ldap-filter
        $filter = $this->_createFilter($criteria, $class);

        set_error_handler([$this, '_ldap_warning_handler'], E_WARNING); // Catch LDAP warning

        $bind = $this->bind[$class::BIND_KEY];
        $fields = $class::_getLdapKey();
        $query = ldap_search($this->connection, $bind, $filter, $fields);

        restore_error_handler();

        if (!$query)
            return false;

        $entries = ldap_get_entries($this->connection, $query);

        $objects = [];

        for ($i = 0; $i < $entries['count']; $i++)
        {
            $object = new $class($entries[$i], $this);
            if ($by_id)
            {
                $objects[$object->id] = $object; // Set by ID
            }
            else
            {
                $objects[] = $object; // Set incrementally
            }
        }

        return $objects;
    }

    /**
     * Find users based on field(s)
     *
     * @param array $criteria   Associated array of criteria, use second level
     *                          for "or"
     * @return LDAPUser[]|false Array of LDAPUser objects
     */
    public function findUsersByValues($criteria)
    {
        return $this->_findObjectsByValues(LDAPUser::class, $criteria, true);
    }

    /**
     * Get all active users (no old members), so type < 4
     *
     * @return LDAPUser[]|false     Array of LDAPUser objects
     */
    public function findAllUsers()
    {
        return $this->_findObjectsByValues(LDAPUser::class, ['membership' => [0, 1, 2, 3]], true);
    }

    /**
     * Get single specified user by primary key
     *
     * @param string|int $id
     * @return LDAPUser|false
     */
    public function getUser($id)
    {
        $users = $this->findUsersByValues(['id' => $id]);

        if (empty($users) || count($users) > 1)
            return false;
        else
            return $users[$id];
    }

    /**
     * Get a list of users by their primary key. Uses only a single LDAP query,
     * and should be efficient than calling `getUser()` multiple times. The
     * function cannot tell which ids failed
     *
     * @param array $ids List of primary ids
     * @return LDAPUser[] Array of ldap user objects
     */
    public function getUsers($ids)
    {
        if (empty($ids))
        {
            return []; // Don't bother searching at all
        }

        return $this->findUsersByValues(['id' => $ids]);
    }

    /**
     * Return single user by username
     *
     * @param string $username
     * @return LDAPUser|false
     */
    public function getUserByUsername($username)
    {
        $users = $this->_findObjectsByValues(LDAPUser::class, ['username' => $username]);

        if (empty($users[0]) || count($users) > 1)
            return false;
        else
            return $users[0];
    }

    /**
     * Get user by student number
     *
     * @param string $number Student number (no 's')
     * @return LDAPUser|false
     */
    public function getUserByStudentnumber($number)
    {
        $users = $this->_findObjectsByValues(LDAPUser::class, ['studentnumber' => $number]);

        if (empty($users[0]) || count($users) > 1)
            return false;
        else
            return $users[0];
    }

    /**
     * Return a new user object
     *
     * @param array|null $data Information, like for patchEntity
     * @return LDAPUser
     */
    static public function createUser($data = null)
    {
        $new_user = new LDAPUser();

        if (!is_null($data))
        {
            $new_user->patchEntity($data);
        }

        return $new_user;
    }

    /**
     * Return a new user object with a link to this LDAPI instance
     *
     * @param array|null $data
     * @return LDAPUser
     */
    public function createUserWithLink($data = null)
    {
        $new_user = new LDAPUser(null, $this);

        if (!is_null($data))
        {
            $new_user->patchEntity($data);
        }

        return $new_user;
    }

    /**
     * Remove a user from the LDAP - Can not be undone!
     *
     * @param LDAPUser $user
     * @return bool
     */
    public function deleteUser($user)
    {
        if ($this->_writeCheck() === false)
            return false;

        $user->collectCommittees(); // Get committees before user is deleted

        $dn = $user->_getDNPrefix() . ',' . $this->bind[LDAPUser::BIND_KEY];

        $email_forward = $user->getEmailForward();

        if (!ldap_delete($this->connection, $dn))
        {
            return false; // Delete failed
        }

        // Remove user from all committees
        foreach ($user->committees as $committee)
        {
            $committee->removeMembers($user);
        }

        // Remove email forwards too (if there was one)
        if ($email_forward)
        {
            $user->saveEmailForward(false);
        }

        return true;
    }

    /**
     * Save a new user to the LDAP
     *
     * @param LDAPUser $user
     * @param string   $password (clear, un-hashed)
     * @return bool
     */
    public function saveNewUser($user, $password)
    {
        if ($this->getUserByUsername($user->username) !== false)
        {
            $user->_setErrors('username', 'A user with this username already exists');
            return false; // Already exists
        }

        $ldap_addition = [
            'userpassword' => '{CRYPT}' . @crypt($password),
            // Suppress crypt(), because it should not be used without salt.
            // However, with our LDAP, a salt couldn't even be used.
            //
            'objectclass' => ['Astatine', 'organizationalPerson', 'posixAccount'],
            'homedirectory' => '/home/user', // Redundant but required
            'gidnumber' => 100, // Essential for posix
            'lidnummer' => 1, // Redundant but required
        ];

        return $this->_saveNewObject($user, $ldap_addition);
    }

    /**
     * Save a new object to the LDAP
     *
     * @param LDAPObject $object
     * @param array|null $additional_fields Additional info (with LDAP keys)
     * @return bool
     */
    public function _saveNewObject($object, $additional_fields = null)
    {
        if ($this->_writeCheck() === false) // Trigger error
            return false;

        // Validate user object, all fields
        if ($object->_validate($object::LDAP_KEYS) === false)
            return false;

        $object->patchEntity([
            'id' => $this->_getNextObjectID(get_class($object))
        ]);

        $ldap_entry = $object->_dirtyToLDAPArray(true); // Cast object to array

        foreach ($additional_fields as $key => $value)
        {
            $ldap_entry[$key] = $value;
        }

        $bind = $object->_getDNPrefix() . ',' . $this->bind[$object::BIND_KEY];

        $object->_clearDirty();

        return ldap_add($this->connection, $bind, $ldap_entry);
    }

    /**
     * Save a modified object to the LDAP
     *
     * @param LDAPObject $object
     * @return bool
     */
    public function _saveUpdatedObject($object)
    {
        if ($this->_writeCheck() === false) // Trigger error
            return false;

        // Validate user object
        if ($object->_validate() === false)
            return false;

        $ldap_modification = $object->_dirtyToLDAPArray();

        $bind = $object->_getDNPrefix() . ',' . $this->bind[$object::BIND_KEY];

        $object->_clearDirty();

        return ldap_modify($this->connection, $bind, $ldap_modification);
    }

    /**
     * Save changes of a user to the LDAP
     *
     * @param LDAPUser $user
     * @return bool
     */
    public function saveUpdatedUser($user)
    {
        return $this->_saveUpdatedObject($user);
    }

    /**
     * Rename an LDAP user
     *
     * @param LDAPUser $user
     * @param string   $new_username
     * @return bool
     */
    public function _saveChangeUsername($user, $new_username)
    {
        if ($this->_writeCheck() === false)
            return false;

        $bind = $user->_getDNPrefix() . ',' . $this->bind[LDAPUser::BIND_KEY];

        $new_bind = 'uid=' . $new_username;

        // Do not change parent and remove old one
        return ldap_rename($this->connection, $bind, $new_bind, null, true);
    }

    /**
     * Change the users password and save to the LDAP
     *
     * @param LDAPUser $user
     * @param string   $newPassword
     * @return bool
     */
    public function _changeUserPassword($user, $newPassword)
    {
        // No write privileges needed

        if (CRYPT_SHA512 == 0)
            trigger_error('System does not support CRYPT_SHA512, refusing to produce unsafe passwords', E_USER_ERROR);

        $salt = $this->_getRandomString(16); // Salt should be 16 characters from [0-9,a-z,A-Z]
        // Using this hash format we specify a CRYPT SHA-512 hash, with a 16
        // character salt, with 5000 hash rounds
        $passwordCrypt = '{CRYPT}' . crypt($newPassword, '$6$rounds=5000$' . $salt . '$');

        $bind = $user->_getDNPrefix() . ',' . $this->bind[LDAPUser::BIND_KEY];

        return ldap_modify($this->connection, $bind, ['userpassword' => $passwordCrypt]);
    }

    /**
     * Return forwarding email for specific user from the forwarding table
     *
     * @param LDAPUser $user
     * @return string|false
     */
    public function _getEmailForward($user)
    {
        $query = ldap_search($this->connection, $this->bind['forward'], "cn=" . $user->username);

        $entry = ldap_get_entries($this->connection, $query);

        if ($entry['count'] === 1)
        {
            return $entry[0]['postofficebox'][0];
        }
        else
            return false;
    }

    /**
     * Edit forwarding email for specific user (errors are placed under 'emailforward')
     *
     * @param LDAPUser $user
     * @param string   $email
     * @return bool
     */
    public function _saveEmailForward($user, $email)
    {
        // No write privileges needed
        //
        // Check email
        if ($email !== false && !filter_var($email, FILTER_VALIDATE_EMAIL))
        {
            $user->_setErrors('emailforward', 'Email is invalid');
            return false;
        }

        $query = ldap_search($this->connection, $this->bind['forward'], "cn=" . $user->username);

        $entry = ldap_get_entries($this->connection, $query);

        // Currently enabled
        if ($entry['count'] === 1) // Found entry
        {
            $current_email = $entry[0]['postofficebox'][0];

            // Already the same, no changes needed
            if ($current_email === $email)
                return null;

            // Modify the entry
            elseif ($email !== false)
                return ldap_modify($this->connection, $entry[0]['dn'], ['postofficebox' => $email]); // Edit and save entry
            else // Delete
                return ldap_delete($this->connection, $entry[0]['dn']);
        } // Currently disabled
        else
        {
            // Already turned off, no changes needed
            if ($email === false)
                return null;
            else
            {
                $new_entry = ['cn' => $user->username, 'objectClass' => ['organizationalPerson', 'top'], 'postOfficeBox' => $email, 'sn' => 'forward'];
                $bind = 'cn=' . $user->username . ',' . $this->bind['forward'];
                return ldap_add($this->connection, $bind, $new_entry);
            }
        }
    }

    /**
     * Get the user photo
     *
     * @param LDAPUser $user
     * @param bool     $thumb           True for small photo, false for large
     * @return string|false             Returns image data as string, simply
     *                                  echo to render
     */
    public function _getUserPhoto($user, $thumb)
    {
        $prefix = LDAPUser::SEARCH_PREFIX;
        $filter = "(&({$prefix})(uid={$user->username}))";
        $query = ldap_search($this->connection, $this->bind[LDAPUser::BIND_KEY], $filter, ['jpegPhoto']);

        if (!$query)
            return false;

        $entries = ldap_get_entries($this->connection, $query);

        if ($thumb == true)
            $key = 1;
        else
            $key = 0;

        return isset($entries[0]['jpegphoto'][$key]) ? $entries[0]['jpegphoto'][$key] : false;
    }

    /**
     * Save new photo to the ldap (errors are placed under 'photo')
     *
     * @param LDAPUser $user
     * @param string   $filepath
     * @return bool
     */
    public function _saveUserPhoto($user, $filepath)
    {
        // Verify image
        $size = getimagesize($filepath);
        if ($size === false)
            return 'File is not a valid image';

        if ($size[0] <= 100 || $size[1] <= 150)
            return 'Image is too small';

        if (mime_content_type($filepath) !== 'image/jpeg')
            return 'File is not of type jpeg';

        $file_large = $this->_image_resize($filepath, 400, 400);
        $file_small = $this->_image_resize($filepath, 100, 150);

        if ($file_large === false || $file_small === false)
            return 'Failed to resize file';

        $jpegPhoto = ['jpegPhoto' => [$file_large, $file_small]];

        // Save to LDAP
        $base = $this->bind[LDAPUser::BIND_KEY];
        $bind = "uid={$user->username},{$base}";
        return ldap_mod_replace($this->connection, $bind, $jpegPhoto);
    }

    /**
     * Find committees based on field(s)
     *
     * @param array $criteria   Associated array of search values, use second
     *                          level for "or"
     * @return LDAPCommittee[]|false  Array of LDAPCommittee objects
     */
    public function findCommitteesByValues($criteria)
    {
        return $this->_findObjectsByValues(LDAPCommittee::class, $criteria, true);
    }

    /**
     * Get all committees
     *
     * @return LDAPCommittee[]|false Array of LDAPCommittee objects
     */
    public function findAllCommittees()
    {
        return $this->_findObjectsByValues(LDAPCommittee::class, [], true);
    }

    /**
     * Get committee based on the id
     *
     * @param string|int $id
     * @return LDAPCommittee|false
     */
    public function getCommittee($id)
    {
        $committees = $this->findCommitteesByValues(['id' => $id]);

        if (count($committees) == 1)
            return $committees[$id];
        else
            return false;
    }

    /**
     * Get committees based on a list of ids. Uses only a single LDAP query, so
     * should be more efficient that calling `getCommittee()` multiple times.
     * There is failure for the function on invalid ids
     *
     * @param array $ids List of primary keys
     * @return LDAPCommittee[]          List of committee objects
     */
    public function getCommittees($ids)
    {
        if (empty($ids))
        {
            return []; // Don't bother searching at all
        }

        return $this->findCommitteesByValues(['id' => $ids]);
    }

    /**
     * Get committees by name
     *
     * @param string $name
     * @return LDAPCommittee|false
     */
    public function getCommitteeByName($name)
    {
        $committees = $this->_findObjectsByValues(LDAPCommittee::class, ['name' => $name]);

        if (count($committees) == 1)
            return $committees[0];
        else
            return false;
    }

    /**
     * Return a new committee object
     *
     * @param array|null $data Information, just like in patchEntity
     * @return LDAPCommittee
     */
    static public function createCommittee($data = null)
    {
        $new_com = new LDAPCommittee();

        if (!is_null($data))
        {
            $new_com->patchEntity($data);
        }

        return $new_com;
    }

    /**
     * Return a new committee object and link it to this LDAPI instance
     *
     * @param array|null $data Information, just like in patchEntity
     * @return LDAPCommittee
     */
    public function createCommitteeWithLink($data = null)
    {
        $new_com = new LDAPCommittee(null, $this);

        if (!is_null($data))
        {
            $new_com->patchEntity($data);
        }

        return $new_com;
    }

    /**
     * Remove a committee from the LDAP - Can not be undone!
     *
     * @param LDAPCommittee $committee
     * @return bool
     */
    public function deleteCommittee($committee)
    {
        if ($this->_writeCheck() === false)
        {
            return false;
        }

        $dn = $committee->_getDNPrefix() . ',' . $this->bind[LDAPCommittee::BIND_KEY];
        if (!ldap_delete($this->connection, $dn))
        {
            return false; // Delete failed
        }

        $aliases = $this->findCommitteesByValues(['member_usernames' => $committee->name]);
        foreach ($aliases as $alias)
        {
            // Find alias committees and remove remote links
            $alias->removeMembers($committee->name);
        }

        return true;
    }

    /**
     * Save a new committee to the LDAP
     *
     * @param LDAPCommittee $committee
     * @return bool
     */
    public function saveNewCommittee($committee)
    {
        if ($this->getCommitteeByName($committee->name) !== false)
        {
            $committee->_setErrors('name', 'A committee with this name already exists');
            return false; // Already exists
        }

        $ldap_addition = [
            'description' => 'com',
            'objectclass' => 'posixGroup'
        ];

        return $this->_saveNewObject($committee, $ldap_addition);
    }

    /**
     * Save changes of a committee to the LDAP
     *
     * @param LDAPCommittee $committee
     * @return bool
     */
    public function saveUpdatedCommittee($committee)
    {
        return $this->_saveUpdatedObject($committee);
    }

    /**
     * Save the renaming of a committee to the LDAP
     *
     * @param LDAPCommittee $committee
     * @param string        $new_name
     * @return bool
     */
    public function _saveRenameCommittee($committee, $new_name)
    {
        if ($this->_writeCheck() === false)
            return false;

        $bind = $committee->_getDNPrefix() . ',' . $this->bind[LDAPCommittee::BIND_KEY];

        $new_bind = 'cn=' . $new_name;

        // Do not change parent and remove old one
        return ldap_rename($this->connection, $bind, $new_bind, null, true);
    }

    /**
     * Paginate the array of objects
     *
     * This cannot be more efficiently by e.g. query modification.
     *
     * The possible options are:
     *   `sort` =>         Field to sort on
     *   `direction` =>    Direction to sort on
     *   `limit` =>        Number of entries per page
     *   `page` =>        The page to return
     *
     * When the last page is reached the page will be smaller than `limit`.
     *
     * @param array $list    Array to be paginated
     * @param array $options List of settings
     * @return int|false Return number of pages or `false` it no pagination was performed
     */
    public function paginate(&$list, $options)
    {
        // Sort
        if (isset($options['sort']))
        {
            if (!isset($options['direction']))
                $options['direction'] = 'asc';

            $sort = strtolower($options['sort']);

            $sign = ($options['direction'] == 'asc' ? 1 : -1);

            usort($list, function ($a, $b) use ($sort, $sign)
            {
                return $sign * strnatcasecmp($a->{$sort}, $b->{$sort});
            });
        }

        // Splice
        if (isset($options['limit']))
        {
            if (!isset($options['page']))
                $options['page'] = 1;

            $limit = $options['limit'];
            $page = $options['page'];

            $page_max = ceil(count($list) / $limit);

            if ($page > $page_max)
                $page = $page_max;

            $list = array_splice($list, ($page - 1) * $limit, $limit);

            return $page_max;
        }

        return false;
    }

    /**
     * Set size limit for this ldap instance
     *
     * Persists between searches, reset to prevent this.
     * Limiting the number of records is not an efficient way of pagination! Simple
     * benchmarks give the impression that PHP-side pagination is faster than LDAP
     * side.
     *
     * @param int|false $limit Size limit, set to false to disable limit
     */
    public function setLimit($limit)
    {
        // No limit
        if ($limit == false)
        {
            ldap_set_option($this->connection, LDAP_OPT_SIZELIMIT, 0);
        }
        else
        {
            ldap_set_option($this->connection, LDAP_OPT_SIZELIMIT, $limit);
        }
    }

    /**
     * Error handler that will simply suppress some LDAP warnings that should not show on the webpage
     *
     * @param $errno
     * @param $errstr
     */
    protected function _ldap_warning_handler($errno, $errstr)
    {
        if ($errno != 2)
        {
            echo "<b>Warning:</b> ({$errno}) {$errstr} <br><br>";
        }
        // Do nothing for $errno == 2 (e.g. user bind failed because of incorrect password)
    }

    /**
     * Shut the LDAP connection, can only be called by the destructor
     *
     * @param void
     * @return void
     */
    protected function _close()
    {
        ldap_close($this->connection);
    }

    /**
     * Create new LDAP connection
     *
     * @param void
     * @return resource|false
     */
    protected function _makeConnection()
    {
        if (isset($this->config['port']))
            $c = ldap_connect($this->config['host'], $this->config['port']);
        else
            $c = ldap_connect($this->config['host']);

        ldap_set_option($c, LDAP_OPT_PROTOCOL_VERSION, 3);

        if ($c === false) // LDAP connection error
        {
            trigger_error('LDAP Connection error...', E_USER_ERROR);
        }

        return $c;
    }

    /**
     * Create a string with an LDAP filter from an array of criteria
     *
     * @param array  $criteria  Associative array with search criteria, use
     *                          second level for "or"
     * @param string $class     Class of the looked-for object
     * @return string           String ready for ldap_search()
     */
    protected function _createFilter($criteria, $class)
    {
        $filter = '(&' . $class::SEARCH_PREFIX;

        foreach ($criteria as $key => $value) // Base level (AND)
        {
            if (is_array($value)) // List of criteria: OR
            {
                $filter .= '(|';
                foreach ($value as $key2 => $value2)
                {
                    if (is_numeric($key2)) // Un-keyed list, chain options together
                        $filter .= $this->_createFilterEntry($key, $value2, $class);
                    else
                        $filter .= $this->_createFilterEntry($key2, $value2, $class);
                }
                $filter .= ')';
            }
            else
                $filter .= $this->_createFilterEntry($key, $value, $class); // Single key-value pair
        }
        $filter .= ')';

        return $filter;
    }

    /**
     * Creates a filter entry from array, transform display keys to ldap keys
     *
     * @param string            $key   Property key
     * @param string            $value Criterion
     * @param string|LDAPObject $class
     * @return string
     */
    protected function _createFilterEntry($key, $value, $class)
    {
        $key_end = substr($key, -2);

        if ($key_end == '>=' || $key_end == '<=')
        {
            $compare = $key_end;
            // Remove last two characters and trim spaces:
            $key = rtrim(substr($key, 0, -2));
        }
        else
        {
            $compare = '=';
        }

        $ldap_key = $class::_getLdapKey($key);

        if ($ldap_key === false)
        {
            return null; // Warning is already thrown by static method
        }

        return "({$ldap_key}{$compare}{$value})";
    }

    /**
     * Find the next incremental primary id
     *
     * @param string $class
     * @return int
     */
    protected function _getNextObjectID($class)
    {
        $all_entries = $this->_findObjectsByValues($class);

        $max_id = 0;

        foreach ($all_entries as $entry)
        {
            if ($entry->id > $max_id)
                $max_id = (int)$entry->id;
        }

        return $max_id + 1;
    }

    /**
     * Resize an image to specified format
     *
     * @param string $source        File path
     * @param int    $width         New width in pixels
     * @param int    $height        New height in pixels
     * @return string|false         Return image data in string format or false
     *                              on error
     */
    protected function _image_resize($source, $width, $height)
    {
        // Get new dimensions
        $size_orig = getimagesize($source);

        $width_orig = $size_orig[0];
        $height_orig = $size_orig[1];

        $ar_orig = $width_orig / $height_orig; // Aspect ratio
        $ar = $width / $height;

        // Maintain correct aspect ratio
        if (is_null($width_orig))
            return false;

        if ($width_orig < $width && $height_orig < $height) // Already smaller
        {
            $width = $width_orig;
            $height = $height_orig;
        }
        elseif ($ar > $ar_orig) // Original is less wide, fix height
        {
            $width = ceil($height * $ar_orig);
        }
        else // Original is wider, fix width
        {
            $height = ceil($width / $ar_orig);
        }

        // Resize
        $img_orig = imagecreatefromjpeg($source);
        $img = imagecreatetruecolor($width, $height);
        imagecopyresampled($img, $img_orig, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);

        // To turn the JPEG into a string we need to output it but capture the output
        ob_start();
        imagejpeg($img);
        $img_string = ob_get_clean();

        imagedestroy($img);
        imagedestroy($img_orig);

        return $img_string;
    }

    /**
     * Produce random string from [0-9, a-z, A-Z]
     *
     * @param int $size = 10 Number of characters
     * @return string Random string
     */
    protected function _getRandomString($size = 10)
    {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $len = strlen($chars);

        $txt = '';
        for ($i = 0; $i < $size; $i++)
        {
            $txt .= $chars[rand(0, $len - 1)];
        }
        return $txt;
    }

    /**
     * Check if instance can perform writes and print error if necessary
     *
     * @param void
     * @return bool True on writable
     */
    protected function _writeCheck()
    {
        if ($this->write_enable === false)
        {
            trigger_error('LDAPI: Attempted to write changes with write lock enabled in the config/ldapi.php - ', E_USER_ERROR);
        }

        return true;
    }

}
