<?php

/**
 * This file contains the Rules class
 */

namespace LDAPI;

use DateTime;
use Exception;

/**
 * Rules class
 *
 * This class contains only static methods. Each method is a validation rule.
 * It designed to be used internally by an LDAPObject.
 *
 * @package     LDAPI
 * @author      ITco <itco@astatine.utwente.nl>
 * @author      Robert Roos <robert.roos@astatine.utwente.nl>
 * @copyright   September 2017 - 2018, S.A. Astatine
 * @license     PHP License 3.01
 */
class Rules
{

    /* static function optional($val)
      {
      if (is_null($val)) // Includes empty string
      return true;
      else
      return false;
      } */

    /**
     * String validation
     *
     * @param mixed $val
     * @return true|string
     */
    static function is_string($val)
    {
        if (!is_string($val))
            return 'Not of type text';

        return true;
    }

    /**
     * Number validation
     *
     * @param mixed $val
     * @return true|string
     */
    static function is_number($val)
    {
        if (!is_numeric($val))
            return 'Not of type number';

        return true;
    }

    /**
     * String length validation
     *
     * @param mixed $val
     * @param array $options [0 => min, 1 => max]
     * @return true|string
     */
    static function string_length($val, $options)
    {
        $min = $options[0];
        $max = $options[1];

        if (strlen($val) < $min || strlen($val) > $max)
            return 'Must be between ' . $min . ' and ' . $max . ' characters';

        return true;
    }

    /**
     * Numeric range validation
     *
     * @param mixed $val
     * @param array $options [0 => min, 1 => max]
     * @return true|string
     */
    static function range($val, $options)
    {
        $min = $options[0];
        $max = $options[1];

        if ($val < $min || $val > $max)
            return 'Value must be between ' . $min . ' and ' . $max;

        return true;
    }

    /**
     * Verify array elements are unique
     *
     * @param array $val
     * @return true|string
     */
    static function array_unique($val)
    {
        $size = count($val);
        $size_unique = count(array_unique($val));

        if ($size === $size_unique)
            return true; // No duplicates
        else
            return 'Items must be unique, no duplicates allowed';
    }

    /**
     * Array validation
     *
     * @param mixed $val
     * @return true|string
     */
    static function is_array($val)
    {
        if (!is_array($val))
            return 'Entry must be an array';

        return true;
    }

    /**
     * IBAN validation
     *
     * @param mixed $val
     * @return true|string
     */
    static function valid_iban($val)
    {
        $iban = strtolower($val);

        $countries = ['al' => 28, 'ad' => 24, 'at' => 20, 'az' => 28, 'bh' => 22, 'be' => 16, 'ba' => 20, 'br' => 29, 'bg' => 22, 'cr' => 21, 'hr' => 21, 'cy' => 28, 'cz' => 24, 'dk' => 18, 'do' => 28, 'ee' => 20, 'fo' => 18, 'fi' => 18, 'fr' => 27, 'ge' => 22, 'de' => 22, 'gi' => 23, 'gr' => 27, 'gl' => 18, 'gt' => 28, 'hu' => 28, 'is' => 26, 'ie' => 22, 'il' => 23, 'it' => 27, 'jo' => 30, 'kz' => 20, 'kw' => 30, 'lv' => 21, 'lb' => 28, 'li' => 21, 'lt' => 20, 'lu' => 20, 'mk' => 19, 'mt' => 31, 'mr' => 27, 'mu' => 30, 'mc' => 27, 'md' => 24, 'me' => 22, 'nl' => 18, 'no' => 15, 'pk' => 24, 'ps' => 29, 'pl' => 28, 'pt' => 25, 'qa' => 29, 'ro' => 24, 'sm' => 27, 'sa' => 24, 'rs' => 22, 'sk' => 24, 'si' => 19, 'es' => 24, 'se' => 24, 'ch' => 21, 'tn' => 24, 'tr' => 26, 'ae' => 23, 'gb' => 22, 'vg' => 24];
        $chars = ['a' => 10, 'b' => 11, 'c' => 12, 'd' => 13, 'e' => 14, 'f' => 15, 'g' => 16, 'h' => 17, 'i' => 18, 'j' => 19, 'k' => 20, 'l' => 21, 'm' => 22, 'n' => 23, 'o' => 24, 'p' => 25, 'q' => 26, 'r' => 27, 's' => 28, 't' => 29, 'u' => 30, 'v' => 31, 'w' => 32, 'x' => 33, 'y' => 34, 'z' => 35];

        $country_code = substr($iban, 0, 2);

        if (!isset($countries[$country_code]))
        {
            return "Unrecognized country code `{$country_code}`";
        }

        if (strlen($iban) != $countries[$country_code])
        {
            $l = $countries[$country_code];
            return "Invalid IBAN length, expecting {$l} characters for country `{$country_code}`";
        }

        $rearranged = substr($iban, 4) . substr($iban, 0, 4);
        $str = '';

        foreach (str_split($rearranged) as $value)
        {
            if (is_numeric($value))
                $str .= $value;
            else
                $str .= $chars[$value];
        }

        if (bcmod($str, '97') != 1)
        {
            return 'Invalid IBAN, control number is incorrect';
        }

        return true;
    }

    /**
     * Phonenumber validation
     *
     * @param mixed $val
     * @return true|string
     */
    static function is_phonenumber($val)
    {
        if (preg_match('/[^0-9.#\\-+$]/', $val) == 1)
            return 'Not a valid phonenumber';

        return true;
    }

    /**
     * Studentnumber validation
     *
     * @param mixed $val
     * @return true|string
     */
    static function is_studentnumber($val)
    {
        // If string contains any non-number character, or if the length is incorrect
        if (preg_match('/[^0-9]/', $val) == 1 || strlen($val) !== 7)
            return 'Not a valid studentnumber';

        return true;
    }

    /**
     * Email validation
     *
     * @param mixed $val
     * @return true|string
     */
    static function is_email($val)
    {
        if (filter_var($val, FILTER_VALIDATE_EMAIL) === false)
            return 'Not a valid email address';

        return true;
    }

    /**
     * Date validation
     *
     * @param mixed $val
     * @return true|string
     */
    static function is_valid_date($val)
    {
        try
        {
            $date = DateTime::createFromFormat('Y-m-d', $val);
        }
        catch (Exception $e)
        {
            return 'Failed to parse input as valid date, the format is `yyyy-mm-dd`';
        }

        if ($date === false)
            return 'Date not valid, the format is yyyy-mm-dd';

        if ($date < new DateTime('1900-01-01'))
            return 'Date is not valid, it is too far in the past';

        if ($date > new DateTime())
            return 'Date is not valid, it cannot be in the future';

        return true;
    }

}
