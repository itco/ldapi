<?php

/**
 * This file contains the LDAPObject class and the rules class
 */

namespace LDAPI;

/**
 * LDAP Object class
 *
 * This class is the parent of the users and committees returned by the LDAP.
 *
 * @package     LDAPI
 * @author      ITco <itco@astatine.utwente.nl>
 * @author      Robert Roos <robert.roos@astatine.utwente.nl>
 * @copyright   September 2017 - 2018, S.A. Astatine
 * @license     PHP License 3.01
 */
class LDAPObject
{

    /**
     * Primary id of this object
     *
     * @var int|null
     */
    public $id = null;

    /**
     * Validation errors, ordered by field as key
     *
     * @var array
     */
    protected $errors = [];

    /**
     * Altered fields, queue to be saved, 'field' => 'new_value'
     *
     * @var array
     */
    protected $dirty = [];

    /**
     * Validators to apply: 'field' => [list of rules]
     *
     * @var array
     */
    protected static $validators = [];

    /**
     * List of fields that cannot be patched directly (when altered directly changes are not saved!)
     *
     * @var string[]
     */
    const FIXED_FIELDS = [];

    /**
     * Link to LDAPI instance
     *
     * @var LDAPI
     */
    protected $ldapi_link;

    /**
     * Maps LDAP keys to properties ('ldap_key' => 'property')
     *
     * @var array
     */
    const LDAP_KEYS = [];

    /**
     * Maps properties to LDAP keys, reverse of LDAP_KEYS: 'property' => 'ldap_keys'
     *
     * This property is static instead of const, because we want to automatically set it based on LDAP_KEYS.
     *
     * @var array
     */
    protected static $DISPLAY_KEYS = [];

    /**
     * Search addition needed for ldap_search (contains object type)
     *
     * @var string
     */
    const SEARCH_PREFIX = '';

    /**
     * Key to the type of bind address (actual bind address is read by LDAPI)
     *
     * @var string
     */
    const BIND_KEY = '';

    /**
     * Initialize object from LDAP result
     *
     * @param array|null $ldap_entry LDAP array
     * @param LDAPI|null $ldapi_link Link to LDAPI instance
     * @return void
     */
    public function __construct($ldap_entry = null, &$ldapi_link = null)
    {
        if (!is_null($ldap_entry))
        {
            // Data passed, convert to user format
            $this->_buildObjectFromLdapData($ldap_entry);
        }

        if ($ldapi_link)
        {
            $this->ldapi_link = &$ldapi_link;
        }
        else
        {
            $this->ldapi_link = false;
        }
    }

    /**
     * Override default debug to make result more clear
     *
     * @return array
     */
    public function __debugInfo()
    {
        $debug = [];
        foreach (array_values(static::LDAP_KEYS) as $field)
        {
            $debug[$field] = $this->{$field};
        }

        $debug['errors'] = $this->errors;
        $debug['dirty'] = $this->dirty;

        $debug['ldapi_link'] = is_object($this->ldapi_link) ? 'ldapi_link' : $this->ldapi_link;

        return $debug;
    }

    /**
     * Alter or insert data, validate it and mark it to be saved
     *
     * @param array $data
     * @return bool      False on validation errors
     */
    public function patchEntity($data)
    {
        foreach ($data as $key => $value)
        {
            if (in_array($key, static::LDAP_KEYS))
            {
                // Skip fixed properties, value must exist
                if (in_array($key, static::FIXED_FIELDS) && !is_null($this->{$key}))
                {
                    //$this->errors[$key][] = 'Cannot be altered regularly';
                    continue;
                }

                $this->{$key} = $value;
                $this->dirty[$key] = $value;
            }
            else
            {
                trigger_error('Invalid LDAPObject property: ' . $key, E_USER_NOTICE);
                unset($data[$key]);
            }
        }

        return $this->_validate();
    }

    /**
     * Verify the content, errors are saved in $this->errors
     *
     * @param array|null $fields    Verify these fields only, if null all
     *                              dirty fields are processed
     * @return bool              False on errors
     */
    public function _validate($fields = null)
    {
        // Do not clear errors for mutation functions
        // $this->errors = []; // Clear existing errors

        if (is_null($fields))
        {
            $fields = array_keys($this->dirty);
        }

        // Each field
        foreach ($fields as $field)
        {
            // Each rule
            foreach (static::$validators[$field] as $rule => $options)
            {
                $val = $this->{$field};

                if ($rule === 'optional')
                {
                    if (self::isEmpty($val))
                    {
                        break; // This field is okay
                    }

                    continue; // Go to next rule
                }
                else
                {
                    if (is_null($val))
                    {
                        $this->errors[$field][] = 'This field cannot be left empty';
                        break;
                    }
                }

                $valid = call_user_func_array([Rules::class, $rule], [$val, $options]); // Return true or error message

                if ($valid !== true)
                    $this->errors[$field][] = $valid;
            }
        }

        return empty($this->errors);
    }

    /**
     * Add validation errors
     *
     * @param string       $field  For property or field
     * @param array|string $errors Error message(s)
     * @return void
     */
    public function _setErrors($field, $errors)
    {
        if ($errors === false || $errors === true)
            return; // No errors

        if (is_string($errors))
        {
            $errors = [0 => $errors]; // Turn single error into array
        }

        foreach ($errors as $error)
        {
            $this->errors[$field][] = $error; // Append error
        }
    }

    /**
     * Return validation errors
     *
     * @param string|null $val      Return errors for this field or return all
     *                              errors
     * @return array|false          Array of errors or false if none exist
     */
    public function getErrors($val = null)
    {
        if (is_null($val))
        {
            if (empty($this->errors))
                return false;
            else
                return $this->errors;
        }
        else
            return isset($this->errors[$val]) ? $this->errors[$val] : false;
    }

    /**
     * Return dirty fields with their new values
     *
     * @param void
     * @return array
     */
    public function _dirty()
    {
        return $this->dirty;
    }

    /**
     * Clear all dirty fields
     *
     * @param void
     * @return void
     */
    public function _clearDirty()
    {
        $this->dirty = []; // Empty array
    }

    // Virtual fields

    /**
     * Return prefix needed for LDAP binds
     *
     * @return string
     */
    public function _getDNPrefix()
    {
        trigger_error('No DN prefix available for bare LDAPObject', E_USER_ERROR);
    }

    // Internal functions

    /**
     * Fill instance according to LDAP data
     *
     * @param array $entry LDAP entry array (single entry!)
     * @return void
     */
    protected function _buildObjectFromLdapData($entry)
    {
        for ($trait = 0; $trait < $entry['count']; $trait++) // Specific trait
        {

            $key = $entry[$trait];

            $c = $entry[$key]['count'];

            if (isset(static::LDAP_KEYS[$key]))
            {
                $real_key = static::LDAP_KEYS[$key];
            }
            else
            {
                trigger_error("Trait '{$key}' is not mapped to a property - ");
                continue; // Skip this trait
            }

            if ($c == 1) // Specific value
            {
                $this->$real_key = $entry[$key][0];
            }
            else
            {
                $new_array = [];
                for ($j = 0; $j < $entry[$key]['count']; $j++)
                {
                    $new_array[$j] = $entry[$key][$j];
                }
                $this->$real_key = $new_array;
            }
        }
    }

    /**
     * Collect all dirty fields and return an array compatible with ldap_save
     *
     * The keys of the array will be ldap keys.
     *
     * @param bool $is_add When true, omit empty fields - When false (default),
     *                     empty fields are set to `[]`
     * @return array
     */
    public function _dirtyToLDAPArray($is_add = false)
    {
        $entry = [];

        foreach ($this->dirty as $object_key => $value)
        {
            $ldap_key = static::_getLdapKey($object_key);

            if (self::isEmpty($value))
            {
                /*
                 * LDAP does not really have empty fields, it will remove fields
                 * instead. Removing fields with ldap_modify is done by writing an
                 * empty array. Trying to write an empty string will result in a
                 * syntax error.
                 * _However_, ldap_add on the other cannot deal with an empty array.
                 * To save an empty field (i.e. no field at all), it must simply not
                 * be present.
                 */
                if ($is_add)
                {
                    continue; // Just skip this field
                }
                else
                {
                    $value = []; // Mark field for deletion
                }
            }

            $entry[$ldap_key] = $value;
        }

        return $entry;
    }

    /**
     * Find a display key (property) based on ldap_key
     *
     * If a key could not be found, false is returned and a warning is triggered.
     *
     * The LDAP_KEYS array contains display keys as values, hence it's used here.
     *
     * @param $ldap_key string|null     Leave empty to return all
     * @return string|false|array
     */
    static function _getDisplayKey($ldap_key = null)
    {
        if (is_null($ldap_key))
        {
            // Things like ldap_search do not accept associative arrays, so take only the values
            return array_values(static::LDAP_KEYS);
        }

        if (isset(static::LDAP_KEYS[$ldap_key]))
        {
            return static::LDAP_KEYS[$ldap_key];
        }
        if (in_array($ldap_key, static::LDAP_KEYS))
        {
            // $ldap_key is already a display_key
            return $ldap_key;
        }

        $class = static::class;
        trigger_error("LDAP key '{$ldap_key}' is not known for class `{$class}`", E_USER_WARNING);
        return false;
    }

    /**
     * Find an ldap key based on a display key (property)
     *
     * If a key could not be found, false is returned and a warning is triggered.
     *
     * The DISPLAY_KEYS array contains ldap keys as values, hence it's used here.
     *
     * @param $display_key string|null  Leave empty to return all
     * @return string|false|array
     */
    static function _getLdapKey($display_key = null)
    {
        if (is_null($display_key))
        {
            // Things like ldap_search do not accept associative arrays, so take only the values
            return array_values(static::$DISPLAY_KEYS);
        }

        if (isset(static::$DISPLAY_KEYS[$display_key]))
        {
            return static::$DISPLAY_KEYS[$display_key];
        }
        if (in_array($display_key, static::$DISPLAY_KEYS))
        {
            // $ldap_key is already a display_key
            return $display_key;
        }

        $class = static::class;
        trigger_error("Property key '{$display_key}' is not known for class `{$class}`", E_USER_WARNING);
        return false;
    }

    /**
     * Return true if (and only if) value is either `null`, `""` or `[]`
     *
     * @param mixed $value
     * @return bool
     */
    static function isEmpty($value)
    {
        return is_null($value) || $value === '' || $value === [];
    }

}