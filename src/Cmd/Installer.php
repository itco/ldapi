<?php

namespace LDAPI\Cmd;

use Composer\Script\Event;

/**
 * Class for composer scripts
 */
class Installer
{
    public static function postInstall(Event $event)
    {
        $io = $event->getIO();
        
        $root = dirname(dirname(__DIR__));
        
        $config = $root . '/config/ldapi.php';
        $defaultConfig = $root . '/config/ldapi.default.php';
        
        if (!file_exists($config))
        {
            copy($defaultConfig, $config);
            $io->write("Created `config/ldapi.php` file");
        }
    }
}
