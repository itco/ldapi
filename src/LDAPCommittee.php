<?php

/**
 * This file contains the LDAPCommittee class
 */

namespace LDAPI;

/**
 * LDAP Committee class
 *
 * Each committee (group) returned from the LDAPI will be an instance of the
 * this class.
 *
 * @package     LDAPI
 * @author      ITco <itco@astatine.utwente.nl>
 * @author      Robert Roos <robert.roos@astatine.utwente.nl>
 * @copyright   September 2017 - 2018, S.A. Astatine
 * @license     PHP License 3.01
 */
class LDAPCommittee extends LDAPObject
{

    /** @var string Committee name */
    public $name = null;

    /** @var array List of member usernames */
    public $member_usernames = null;

    /** @var LDAPUser[]|null This array consists of LDAPUser objects, it can be filled with collectMembers() */
    public $members = null;

    const LDAP_KEYS = [
        // LDAP Required
        'cn' => 'name',
        'gidnumber' => 'id',
        // LDAP Optional
        'memberuid' => 'member_usernames'
    ];

    /**
     * Redeclare keys so they are separate from the parent class
     *
     * @see LDAPObject::$DISPLAY_KEYS
     * @var array
     */
    protected static $DISPLAY_KEYS = [];

    const SEARCH_PREFIX = '(objectClass=posixGroup)(description=com)';

    const BIND_KEY = 'groups';

    // The following is a list of the rules applicable to each field
    protected static $validators = [
        'id' => [
            'optional' => [],
            'is_number' => []
        ],
        'name' => [
            'is_string' => [],
            'string_length' => [4, 99]
        ],
        'member_usernames' => [
            'optional' => [],
            'is_array' => []
        ]
    ];

    /**
     * @var string[] The following is a list of fields that cannot be changed regularly
     * and are NOT SAVED when changed anyway
     */
    const FIXED_FIELDS = [
        'id',
        'name',
        'member_usernames'
    ];

    /**
     * Extend parent construct, make sure member_usernames array is actually an array
     *
     * @param array|null $ldap_entry LDAP array
     * @param LDAPI|null $ldapi_link Link to LDAPI instance
     */
    public function __construct($ldap_entry = null, &$ldapi_link = null)
    {
        parent::__construct($ldap_entry, $ldapi_link);

        if (is_string($this->member_usernames))
            $this->member_usernames = [0 => $this->member_usernames];
        elseif (is_null($this->member_usernames))
            $this->member_usernames = [];
    }

    /**
     * Initialize protected static values
     */
    public static function init()
    {
        LDAPCommittee::$DISPLAY_KEYS = array_flip(LDAPCommittee::LDAP_KEYS);
    }

    /**
     * Response when object is used as string (return name)
     *
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * Override the standard PHP debug information to skip boring stuff
     */
    public function __debugInfo()
    {
        $debug = parent::__debugInfo();

        $debug['members'] = $this->members;

        return $debug;
    }

    /**
     * Collect user object of members and save to property
     *
     * @return void
     */
    public function collectMembers()
    {
        $this->members = $this->ldapi_link->findUsersByValues(
            ['username' => $this->member_usernames]
        );
    }

    /**
     * Add new member(s) to a committee and save it to the LDAP, also works on
     * committee aliasing, pass committee name in this case
     *
     * The method will return true if at least one update has been made. It no new members were passed or checks failed
     * for each entry, the method will return false.
     *
     * @param string|string[]|LDAPUser|LDAPUser[] $new_members Array or single, username or LDAPUser object
     * @param bool                                $check       False to disable check, useful for external aliases
     * @return bool
     */
    public function addMembers($new_members, $check = true)
    {
        if (!is_array($new_members))
        {
            $new_members = [$new_members];
        }

        $errors = [];
        $changed = false; // Check if any members are actually going to be added

        foreach ($new_members as $new_member)
        {
            $uid = is_string($new_member) ? $new_member : $new_member->username;

            if (in_array($uid, $this->member_usernames))
            {
                $errors[][$uid] = 'Already in this committee';
                continue; // Skip this user
            }

            if ($check)
            {
                $user = $this->ldapi_link->getUserByUsername($uid);
                if ($user === false)
                {
                    $committee = $this->ldapi_link->getCommitteeByName($uid);
                    if ($committee === false)
                    {
                        $errors[][$uid] = 'User does not exist'; // User nor committee was found
                        continue; // Skip this user
                    }

                    if (!$committee->validateMemberRecursion($this->name))
                    {
                        $errors[][$uid] = 'Committee cannot be a member of itself via other committee(s)';
                        continue; // Skip this user
                    }
                }
            }

            $changed = true;
            $this->member_usernames[] = $uid;
        }

        if ($changed)
        {
            $this->dirty['member_usernames'] = $this->member_usernames;

            // To allow partial saving, the errors are added after saving
            $success = $this->ldapi_link->saveUpdatedCommittee($this);
        }
        else
        {
            // No changes were made, assume everything had errors
            $success = false;
        }

        $this->_setErrors('members', $errors);

        return $success;
    }

    /**
     * Checks if new uid will create circular reference, which might break the mail server
     *
     * This method should be called from committee that is to be added, _not_ from the modified committee.
     * Note: this method will also return false when an existing circle has been found, even if it's outside the new
     * alias that's being checked.
     *
     * @param string        $root_id               Name of committee that should not be in any of the other committees
     * @param null|string[] $committees_check_list List of all the committees already checked, used to detect an
     *                                             existing circular dependency - Leave at default when calling the
     *                                             method for the first time
     * @return bool Returns false when there is a recursive problem
     */
    private function validateMemberRecursion($root_id, &$committees_check_list = null)
    {
        if (!$committees_check_list)
        {
            $committees_check_list = [$root_id];
        }

        if (in_array($root_id, $this->member_usernames))
        {
            return false; // Modified committee is a direct member of the alias committee
        }
        foreach ($this->member_usernames as $member_username)
        {
            if (in_array($member_username, $committees_check_list))
            {
                // Existing circular relation. Best to block the new alias.
                return false;
            }

            // Find all committees in this committee's member_usernames
            $committee = $this->ldapi_link->getCommitteeByName($member_username);
            if ($committee !== false)
            {
                $committees_check_list[] = $committee->name; // Append to list
                if (!$committee->validateMemberRecursion($root_id, $committees_check_list))
                {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Remove member(s) from a committee and save it to the LDAP
     *
     * @param string|string[]|LDAPUser|LDAPUser[] $old_members List or single item,
     *                                                         username or LDAPUser
     * @return bool
     */
    public function removeMembers($old_members)
    {
        if (!is_array($old_members))
            $old_members = [0 => $old_members];

        $members = $this->member_usernames;

        $errors = [];

        foreach ($old_members as $old_member)
        {
            $uid = is_string($old_member) ? $old_member : $old_member->username;
            $key = array_search($uid, $members);
            if ($key === false)
                $errors[][$uid] = 'User is not in this committee';
            else
                unset($members[$key]);
        }

        $this->member_usernames = array_values($members); // Make sure the keys are consecutive
        $this->dirty['member_usernames'] = $this->member_usernames;

        // To allow partial modification, the errors are added later.
        $success = $this->ldapi_link->saveUpdatedCommittee($this);

        $this->_setErrors('members', $errors);

        return $success;
    }

    /**
     * Change the name of the committee and save it to the LDAP (recursively,
     * aliases are modified accordingly) (errors are saved under 'name')
     *
     * @param string $new_name
     * @return bool
     */
    public function saveRename($new_name)
    {
        if ($this->ldapi_link->getCommitteeByName($new_name) !== false)
        {
            $this->_setErrors('name', 'Committee with this name already exists');
            return false;
        }

        // Get linked committees
        $old_name = $this->name;
        $aliases = $this->ldapi_link->findCommitteesByValues(['member_usernames' => $old_name]);

        if ($this->ldapi_link->_saveRenameCommittee($this, $new_name))
        {
            $this->name = $new_name; // Update name in object as well

            foreach ($aliases as $alias)
            {
                $alias->removeMembers($old_name);
                $alias->addMembers($new_name);
            }

            return true;
        }
        else
            return false;
    }


    // Virtual fields

    /**
     * Return prefix needed for LDAP binds
     *
     * @return string
     */
    public function _getDNPrefix()
    {
        return 'cn=' . $this->name;
    }

}

/*
 * Set display keys immediately after class creation
 */
LDAPCommittee::init();
