<?php

/**
 * This file contains the LDAPUser class
 */

namespace LDAPI;

use DateTime;
use Exception;

/**
 * LDAP User class
 *
 * This is the user class. Each user returning from the LDAPI will be an
 * instance of this class. It extends LDAPObject
 *
 * @package     LDAPI
 * @author      ITco <itco@astatine.utwente.nl>
 * @author      Robert Roos <robert.roos@astatine.utwente.nl>
 * @copyright   September 2017 - 2018, S.A. Astatine
 * @license     PHP License 3.01
 */
class LDAPUser extends LDAPObject
{

    // Properties of the user
    public $address = null;
    public $attentie = null;
    public $authorization = null;
    public $bic = null;
    public $birthdate = null;
    public $city = null;
    public $class = null;
    public $country = null;
    public $email = null;
    public $emailaliases = null;
    public $firstname = null;
    public $iban = null;
    public $initials = null;
    public $insertions = null;
    public $joindate = null;
    public $lastname = null;
    public $membership = null;
    public $names = null;
    public $nationality = null;
	public $personalemail = null;
    public $phonenumber = null;
    public $phonenumber_home = null;
    public $postalcode = null;
    public $studentnumber = null;
    public $username = null;

    /**
     * Array of committees, can be filled by calling ->collectCommittees()
     *
     * @var LDAPCommittee[]|null $committees
     */
    public $committees = null;

    const LDAP_KEYS = [
        // LDAP Required
        'c' => 'country',
        'cn' => 'firstname',
        'geboortedatum' => 'birthdate',
        'givenname' => 'names',
        'initials' => 'initials',
        'joindate' => 'joindate',
        'l' => 'city',
        'lidmaatschap' => 'membership',
        'mail' => 'email',
        'mailvoorvoegsel' => 'emailaliases',
		'personalemail' => 'personalemail',
        'postaladdress' => 'address',
        'postalcode' => 'postalcode',
        'sn' => 'lastname',
        'studentnummer' => 'studentnumber',
        'uid' => 'username',
        'attentie' => 'attentie', // Optional for LDAP, but required now
        // LDAP Optional
        'bic' => 'bic',
        'ibanreknummer' => 'iban',
        'lichting' => 'class',
        'machtiging' => 'authorization',
        'mobile' => 'phonenumber',
        'nationaliteit' => 'nationality',
        //'reknummer' => 'bankaccount', // Should be redundant
        'telephonenumber' => 'phonenumber_home',
        'tv' => 'insertions',
        'uidnumber' => 'id',
    ];

    /**
     * Redeclare keys so they are separate from the parent class
     *
     * @see LDAPObject::$DISPLAY_KEYS
     * @var array
     */
    protected static $DISPLAY_KEYS = [];

    const SEARCH_PREFIX = '(objectClass=Astatine)';

    const BIND_KEY = 'people';

    protected static $validators = [
        'attentie' => [
            'is_number' => [],
            'range' => [0, 1]
        ],
        'address' => [
            'is_string' => [],
            'string_length' => [1, 99]
        ],
        'authorization' => [
            'optional' => [],
            'is_number' => []
        ],
        'bic' => [
            'optional' => [],
            'is_string' => [],
            'string_length' => [8, 11]
        ],
        'birthdate' => [
            'is_string' => [],
            'string_length' => [8, 10],
            'is_valid_date' => []
        ],
        'city' => [
            'is_string' => [],
            'string_length' => [2, 30]
        ],
        'class' => [
            'optional' => [],
            'is_number' => [],
            'range' => [1, 999]
        ],
        'country' => [
            'is_string' => [],
            'string_length' => [2, 2] // Currently 'c' (country) must be a two letter code!
        ],
        'email' => [
            'is_email' => []
        ],
        'personalemail' => [
            'is_email' => []
        ],
        'emailaliases' => [
            'is_array' => [],
            'array_unique' => []
        ],
        'firstname' => [
            'is_string' => [],
            'string_length' => [1, 50]
        ],
        'iban' => [
            'optional' => [],
            'is_string' => [],
            'valid_iban' => []
        ],
        'id' => [
            'optional' => [],
            'is_number' => []
        ],
        'initials' => [
            'is_string' => [],
            'string_length' => [1, 10]
        ],
        'insertions' => [
            'optional' => [],
            'is_string' => [],
            'string_length' => [1, 20]
        ],
        'joindate' => [
            'optional' => [],
            'is_string' => [],
            'string_length' => [8, 10],
            'is_valid_date' => []
        ],
        'lastname' => [
            'is_string' => [],
            'string_length' => [2, 50]
        ],
        'membership' => [
            'is_number' => [],
            'range' => [0, 4]
        ],
        'names' => [
            'is_string' => [],
            'string_length' => [1, 50]
        ],
        'nationality' => [
            'optional' => [],
            'is_string' => [],
            'string_length' => [1, 30]
        ],
        'phonenumber' => [
            'optional' => [],
            'is_phonenumber' => []
        ],
        'phonenumber_home' => [
            'optional' => [],
            'is_phonenumber' => []
        ],
        'postalcode' => [
            'is_string' => [],
            'string_length' => [1, 7]
        ],
        'studentnumber' => [
            'is_studentnumber' => []
        ],
        'username' => [
            'is_string' => [],
            'string_length' => [5, 50]
        ],
    ];

    const FIXED_FIELDS = [
        'id',
        'username'
    ];

    /**
     * Different member types - The type is saved as an integer in the LDAP
     */
    const MEMBERSHIP_TYPES = [
        0 => 'Regular membership',
        1 => 'Extraordinary membership',
        2 => 'Alumnus',
        3 => 'Employee membership',
        4 => 'Old member'
    ];

    /**
     * Different financial authorization types - The type is saved as an integer in the LDAP
     */
    const AUTHORIZATION_TYPES = [
        0 => 'None',
        1 => 'Membership',
        2 => 'ASS',
        3 => 'Both',
        4 => 'Everything'
    ];

    /**
     * Different values for sex - The type is saved as an integer in the LDAP
     * 
     * @deprecated The sex/gender/geslacht field has been removed from the LDAP!
     */
    const SEXES = [
        0 => 'Male',
        1 => 'Female',
        2 => 'Undisclosed'
    ];

    /**
     * Year of class 1 (first _lichting_ of AT)
     */
    const FIRST_CLASS = 2004;

    /**
     * Extend parent construct, make sure aliases array is actually an array
     *
     * @param array|null $ldap_entry
     * @param LDAPI|null $ldapi_link Link to LDAPI instance
     * @return void
     */
    public function __construct($ldap_entry = null, &$ldapi_link = null)
    {
        parent::__construct($ldap_entry, $ldapi_link);

        if (is_string($this->emailaliases))
        {
            $this->emailaliases = [$this->emailaliases];
        }
    }

    /**
     * Initialize protected static values
     */
    public static function init()
    {
        LDAPUser::$DISPLAY_KEYS = array_flip(LDAPUser::LDAP_KEYS);
    }

    /**
     * Response when object is used as string (return full name)
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getFullname();
    }

    public function __debugInfo()
    {
        $debug = parent::__debugInfo();

        $debug['committees'] = $this->committees;

        return $debug;
    }

    /**
     * Return forwarding email from the forwarding table
     *
     * @return string|false
     */
    public function getEmailForward()
    {
        return $this->ldapi_link->_getEmailForward($this);
    }

    /**
     * Edit forwarding email (errors are placed under 'emailforward')
     *
     * @param string $email
     * @return bool
     */
    public function saveEmailForward($email)
    {
        return $this->ldapi_link->_saveEmailForward($this, $email);
    }

    /**
     * Change password and save to the LDAP
     *
     * @param string      $newPassword
     * @param string|null $currentPassword      Test old password first,
     *                                          cancel change on failure
     * @return bool
     */
    public function changePassword($newPassword, $currentPassword = null)
    {
        if (!is_null($currentPassword) && $this->ldapi_link->login($this->username, $currentPassword) == false)
        {
            $this->_setErrors('password', 'Current password is incorrect');
        }

        if (strlen($newPassword) < 8)
        {
            $this->_setErrors('password', 'New password is too short, it must be at least 8 characters');
        }

        if (strlen($newPassword) > 100)
        {
            $this->_setErrors('password', '1337 h4xz0r, your password cannot contain more than 100 characters');
        }

        if (is_numeric($newPassword))
        {
            $this->_setErrors('password', 'Your password cannot consist only of numbers');
        }

        if (!empty($this->getErrors('password')))
        {
            return false;
        }

        return $this->ldapi_link->_changeUserPassword($this, $newPassword);
    }

    /**
     * Get the current committees, committees are placed under $this->committees
     *
     * @return void
     */
    public function collectCommittees()
    {
        $this->committees = $this->ldapi_link->findCommitteesByValues(['member_usernames' => $this->username]);
    }

    /**
     * Get photo
     *
     * @param bool $thumb               True for small photo, false for large
     * @return string|false             Returns image data as string, simply
     *                                  echo to render
     */
    public function getPhoto($thumb = false)
    {
        return $this->ldapi_link->_getUserPhoto($this, $thumb);
    }

    /**
     * Save new photo to the ldap (errors are placed under 'photo')
     *
     * @param string $filepath
     * @return bool
     */
    public function savePhoto($filepath)
    {
        $result = $this->ldapi_link->_saveUserPhoto($this, $filepath);

        $this->_setErrors('photo', $result);

        if ($result === true)
            return true; // No errors
        else
            return false; // Errors
    }

    /**
     * Rename an LDAP user (recursive: committees are modified as well)
     * (validation is done, errors are placed under 'username')
     *
     * @param string $new_username
     * @return bool
     */
    public function saveChangeUsername($new_username)
    {
        if ($this->ldapi_link->getUserByUsername($new_username) !== false)
        {
            $this->_setErrors('username', 'A user with this username already exists');
            return false;
        }

        $old_username = $this->username;

        $this->collectCommittees(); // Get committees BEFORE renaming

        if ($this->ldapi_link->_saveChangeUsername($this, $new_username))
        {
            $this->username = $new_username; // Modify instance too
            // Modify main email as well, which is an alias
            $emails = $this->emailaliases;

            $pos = array_search($old_username, $emails);

            if ($pos !== false)
                unset($emails[$pos]);

            $emails[] = $new_username;

            $this->patchEntity(['emailaliases' => array_values($emails)]);
            $this->ldapi_link->saveUpdatedUser($this);

            // Modify all committees user is a part in as well!
            foreach ($this->committees as $committee)
            {
                $committee->removeMembers($old_username);
                $committee->addMembers($new_username);
            }

            return true;
        }
        else
            return false;
    }


    // Virtual fields

    //

    /**
     * @return string
     */
    public function getFullname()
    {
        return $this->firstname . ' ' .
            (is_null($this->insertions) ? '' : ($this->insertions . ' ')) .
            $this->lastname;
    }

    /**
     * @return string
     */
    public function getFormalname()
    {
        return $this->initials . ' ' .
            (is_null($this->insertions) ? '' : ($this->insertions . ' ')) .
            $this->lastname;
    }

    /**
     * @return string
     * @deprecated Sex/gender/geslacht is no longer stored in the LDAP
     */
    public function getSex()
    {
        trigger_error('The gender field has been removed from the LDAP!', E_USER_DEPRECATED);
        return 'Unknown';
    }

    /**
     * @return DateTime
     */
    public function getBirthdate()
    {
        return $this->getDate('birthdate');
    }

    /**
     * @return DateTime
     */
    public function getJoindate()
    {
        return $this->getDate('joindate');
    }

    /**
     * Convert field to DateTime object
     *
     * @param string $key
     * @return DateTime|null
     */
    protected function getDate($key)
    {
        try
        {
            return new DateTime($this->$key);
        }
        catch (Exception $e)
        {
            // On the off chance the saved date is invalid
            return null;
        }
    }

    /**
     * @return string
     */
    public function _getDNPrefix()
    {
        return 'uid=' . $this->username;
    }

    /**
     * @return string
     */
    public function getAuthorization()
    {
        if ($this->authorization < 0 || $this->authorization > count(self::AUTHORIZATION_TYPES))
        {
            return 'Unknown';
        }

        return self::AUTHORIZATION_TYPES[$this->authorization];
    }

    /**
     * @return string
     */
    public function getMembership()
    {
        if ($this->membership < 0 || $this->membership > count(self::MEMBERSHIP_TYPES))
        {
            return 'Unknown';
        }

        return self::MEMBERSHIP_TYPES[$this->membership];
    }

    /**
     * @return string
     */
    public function getAstatineEmail()
    {
        return $this->username . '@astatine.utwente.nl';
    }

    /**
     * @return string
     */
    public function getClassYear()
    {
        $class = (int)$this->class;

        $year = self::FIRST_CLASS + $class - 1; // Class ('lichting') 1 started in 2004
        $year_p1 = $year + 1;

        return "{$year}/{$year_p1}"; // Class 1 is the class of the lecture year 2004/2005
    }

    /**
     * Compute the current class ('lichting'), based on the current date
     *
     * @return int
     */
    public static function getCurrentClass()
    {
        $first_year = self::FIRST_CLASS;
        $first = new DateTime("01-07-{$first_year}"); // Consider the first of july the start of a new lecture year

        $now = new DateTime();

        return $now->diff($first)->y + 1; // Return number of years difference (plus account for 1-basing)
    }

}

/*
 * Set display keys immediately after class creation
 */
LDAPUser::init();
